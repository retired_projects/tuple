package com.tupletheapp.tuple.async_tasks;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.activities.CameraActivity;
import com.tupletheapp.tuple.activities.CreateEventActivity;
import com.tupletheapp.tuple.custom_java.Event;
import com.tupletheapp.tuple.custom_java.EventContent;
import com.tupletheapp.tuple.utils.AmazonUtil;
import com.tupletheapp.tuple.utils.BitmapUtil;
import com.tupletheapp.tuple.utils.IOUtil;
import com.tupletheapp.tuple.utils.MediaUtil;
import com.tupletheapp.tuple.utils.QRCodeUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by ghigareda on 2/8/2016.
 */
public class UploadContentTask extends AsyncTask<EventContent, Void, EventContent> {
    private Activity activity;
    private TransferUtility transferUtility;
    private String s3path;
    private AmazonS3Client client;

    private UploadContentListener mListener;

    public UploadContentTask(Activity activity, UploadContentListener mListener) {
        this.activity = activity;

        this.mListener = mListener;
        transferUtility = AmazonUtil.getTransferUtility(activity);
        client = AmazonUtil.getS3Client(activity);
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected EventContent doInBackground(EventContent... eventContent) {
        return savePhoto(eventContent[0]);
    }

    private EventContent savePhoto(final EventContent eventContent){
        s3path = AmazonUtil.createS3Key("Tuple");
        final File file = MediaUtil.getInstance().bitmapToFile(
                BitmapUtil.newInstance().getRotatedBitmap(eventContent.getUrl()), "content");

        TransferObserver observer = transferUtility.upload(activity.getResources().getString(R.string.bucket_name), s3path, file);
        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state == TransferState.COMPLETED) {
                    String url = client.getResourceUrl(activity.getResources().getString(R.string.bucket_name), s3path);
                    eventContent.setUrl(url);
                    saveEventContent(eventContent);
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

            }

            @Override
            public void onError(int id, Exception ex) {

            }
        });

        return eventContent;
    }


    private void saveEventContent(EventContent eventContent){
        DynamoDBMapper mapper = AmazonUtil.getMapper(activity);
        mapper.save(eventContent);
        Log.e("id", eventContent.getID());
    }


    @Override
    protected void onPostExecute(EventContent content) {
        mListener.finishSave(content);
    }

    public interface UploadContentListener{
        public void finishSave(EventContent eventContent);
    }

}
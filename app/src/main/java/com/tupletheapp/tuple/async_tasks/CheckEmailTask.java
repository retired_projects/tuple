package com.tupletheapp.tuple.async_tasks;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.tupletheapp.tuple.activities.CreateEventActivity;
import com.tupletheapp.tuple.custom_java.User;
import com.tupletheapp.tuple.utils.AmazonUtil;
import com.tupletheapp.tuple.utils.IOUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by ghigareda on 2/5/2016.
 */
public class CheckEmailTask extends AsyncTask<String, Void, Boolean> {
    private Activity activity;

    public CheckEmailTask(Activity activity) {
        this.activity = activity;
    }

    @Override
    protected Boolean doInBackground(String... email) {
        DynamoDBMapper mapper = AmazonUtil.getMapper(activity);
        return mapper.load(User.class, email) == null;
    }

    @Override
    protected void onPostExecute(Boolean exists) {
        if(exists){
            Log.w("Log", "username is taken please try again");
        }else{
            Log.w("Log", "username is available");
        }
    }

}
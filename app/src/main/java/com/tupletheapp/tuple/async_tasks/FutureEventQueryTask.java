package com.tupletheapp.tuple.async_tasks;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBQueryExpression;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.PaginatedQueryList;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.tupletheapp.tuple.custom_java.Attending;
import com.tupletheapp.tuple.custom_java.Event;
import com.tupletheapp.tuple.custom_java.User;
import com.tupletheapp.tuple.utils.AmazonUtil;

import java.util.ArrayList;

/**
 * Created by ghigareda on 2/6/2016.
 */
public class FutureEventQueryTask extends AsyncTask<Void, Void, ArrayList<Event>> {
    private Activity activity;
    private DynamoDBMapper mapper;
    private PaginatedQueryList<Attending> result;
    private ArrayList<Event> futureEvents;

    private OnQueryListener mListener;

    private int loadLimit;

    public FutureEventQueryTask(Activity activity, OnQueryListener mListener, int loadLimit) {
        this.activity = activity;
        this.mListener = mListener;
        this.loadLimit = loadLimit;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mListener.startLoading();
        mapper = new DynamoDBMapper(AmazonUtil.getDynamoDBClient(activity));
        futureEvents = new ArrayList<>();
    }

    @Override
    protected ArrayList<Event> doInBackground(Void... params) {
        String date = "1454692115389";

        Attending attending = new Attending();
        attending.setUserId("1");
        Condition rangeKeyCondition = new Condition()
                .withComparisonOperator(ComparisonOperator.GT.toString())
                .withAttributeValueList(new AttributeValue().withS(date));

        DynamoDBQueryExpression queryExpression = new DynamoDBQueryExpression()
                .withHashKeyValues(attending)
                .withRangeKeyCondition("Event_Date", rangeKeyCondition)
                .withConsistentRead(false);

        result = mapper.query(Attending.class, queryExpression);

        if (result.size() > 0) {
            if (result.size() < 10) {
                loadLimit = result.size();
                mListener.setLoadLimit(result.size());
            }

            for (int i = 0; i < loadLimit; i++)
                futureEvents.add(mapper.load(Event.class, result.get(i).getEventId()));

            mListener.setCurrentlyLoading(loadLimit);
        }

        return futureEvents;
    }

    @Override
    protected void onPostExecute(ArrayList<Event> events){
        mListener.stopLoading();
        if (futureEvents == null){

        }
        else if(futureEvents.isEmpty())
            mListener.displayNoResultsFound();
        else
            mListener.addEvents(events);
    }

    public interface OnQueryListener{
        public void setLoadLimit(int loadLimit);
        public void setCurrentlyLoading(int currentlyLoading);
        public void addEvents(ArrayList<Event> events);
        public void displayNoResultsFound();
        public void startLoading();
        public void stopLoading();
        public void onError();
    }
}

package com.tupletheapp.tuple.async_tasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.tupletheapp.tuple.activities.CameraActivity;
import com.tupletheapp.tuple.activities.CreateEventActivity;
import com.tupletheapp.tuple.utils.BitmapUtil;
import com.tupletheapp.tuple.utils.IOUtil;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class SavePhotoTask extends AsyncTask<Bitmap, Void, String> {
    private Activity activity;
    private PhotoSaveInteractionListener mListener;

    public SavePhotoTask(Activity activity, PhotoSaveInteractionListener mListener){
        this.activity = activity;
        this.mListener = mListener;
    }

    @Override
    protected void onPreExecute() {
        if(activity instanceof CreateEventActivity)
            ((CreateEventActivity) activity).showDialog();
        else if(activity instanceof CameraActivity)
            ((CameraActivity) activity).showDialog();
    }

    @Override
    protected String doInBackground(Bitmap... image) {
        FileOutputStream output = null;
        File mFile = IOUtil.getInstance().createImageFile();
        try {
            output = new FileOutputStream(mFile);
            output.write(BitmapUtil.newInstance().getByteArray(image[0]));
        } catch (IOException e) {
            Log.e("ImageSaver", e.toString());
        } finally {

            if (null != output) {
                try {
                    output.close();
                } catch (IOException e) {
                    Log.e("ImageSaverIO", e.toString());
                }
            }
        }

        addPictureToGallery(mFile.getAbsolutePath());
        return mFile.getAbsolutePath();
    }

    @Override
    protected void onPostExecute(String imagePath) {
        if(activity instanceof CreateEventActivity)
            ((CreateEventActivity) activity).dismissDialog();
        mListener.PhotoSaved(imagePath);
    }

    private void addPictureToGallery(String imagePath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(imagePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        activity.sendBroadcast(mediaScanIntent);
    }

    public interface PhotoSaveInteractionListener {
        public void PhotoSaved(String coverPhotoPath);
    }
}


package com.tupletheapp.tuple.async_tasks;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.tupletheapp.tuple.custom_java.Event;
import com.tupletheapp.tuple.utils.AmazonUtil;

/**
 * Created by sebastian on 2/8/2016.
 */
public class EventInformationQueryTask extends AsyncTask<String , Void , Event> {

    private Activity activity;
    private DynamoDBMapper mapper;
    private QueryListener mListener;

    public  EventInformationQueryTask( Activity activity,QueryListener mListener){
        this.activity=activity;
        this.mListener = mListener;
    }

    @Override
    protected void onPreExecute() {
        mapper = new DynamoDBMapper(AmazonUtil.getDynamoDBClient(activity));
    }

    @Override
    protected Event doInBackground(String... eventID) {
        return mapper.load(Event.class,eventID[0]);
    }

    @Override
    protected void onPostExecute(Event event) {
        mListener.onComplete(event);
    }

    public interface QueryListener{
        public void onComplete(Event event);

    }

}

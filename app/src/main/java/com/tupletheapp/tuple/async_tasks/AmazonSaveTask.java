package com.tupletheapp.tuple.async_tasks;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.s3.AmazonS3Client;
import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.activities.CreateEventActivity;
import com.tupletheapp.tuple.custom_java.Attending;
import com.tupletheapp.tuple.custom_java.Event;
import com.tupletheapp.tuple.utils.AmazonUtil;
import com.tupletheapp.tuple.utils.BitmapUtil;
import com.tupletheapp.tuple.utils.IOUtil;
import com.tupletheapp.tuple.utils.MediaUtil;
import com.tupletheapp.tuple.utils.QRCodeUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class AmazonSaveTask extends AsyncTask<Event, Void, String> {
    private CreateEventActivity activity;
    private NotificationInteractionListener mListener;
    private TransferUtility transferUtility;
    private String s3path;

    private AmazonS3Client client;
//    private DynamoDBMapper mapper;

    private String eventId;

    public AmazonSaveTask(CreateEventActivity activity) {
        this.activity = activity;

        transferUtility = AmazonUtil.getTransferUtility(activity);
        client = AmazonUtil.getS3Client(activity);
    }

    @Override
    protected String doInBackground(Event... event) {
        saveCoverPhoto(event[0]);

        return "string";
    }

    private void saveCoverPhoto(final Event event){
        s3path = AmazonUtil.createS3Key("Tuple");
        final File file = MediaUtil.getInstance().bitmapToFile(
                BitmapUtil.newInstance().getRotatedBitmap(event.getCoverPhotoPath()), "content");

        TransferObserver observer = transferUtility.upload(activity.getResources().getString(R.string.bucket_name), s3path, file);
        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state == TransferState.COMPLETED) {
                    String url = client.getResourceUrl(activity.getResources().getString(R.string.bucket_name), s3path);
                    event.setCoverPhotoUrl(url);
                    saveEvent(event);
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

            }

            @Override
            public void onError(int id, Exception ex) {

            }
        });
    }

    private void saveQRCode(final Event event, Bitmap QRCode,final DynamoDBMapper mapper){
        s3path = AmazonUtil.createS3Key("Tuple");
        final File file = MediaUtil.getInstance().bitmapToFile(QRCode, "content");

        TransferObserver observer = transferUtility.upload(activity.getResources().getString(R.string.bucket_name), s3path, file);
        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state == TransferState.COMPLETED) {
                    Log.e("Completed", "Called");
                    String url = client.getResourceUrl(activity.getResources().getString(R.string.bucket_name), s3path);
                    saveQrCode(event, url, mapper);

//                    saveEvent(event);
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

            }

            @Override
            public void onError(int id, Exception ex) {

            }
        });
    }

    private void saveEvent(Event event){
        DynamoDBMapper mapper = AmazonUtil.getMapper(activity);
        mapper.save(event);
        followOwnEvent(event, mapper);
        generateQRCode(event, mapper);
        eventId = event.getID();
    }

    private void generateQRCode(Event event, DynamoDBMapper mapper){
        Bitmap QRCode = QRCodeUtil.getInstance().generateQRCode(event.getID());
        saveQRCode(event, QRCode, mapper);
    }

    private void followOwnEvent(Event event, DynamoDBMapper mapper){
        Attending attending = new Attending();
        attending.setEventId(event.getID());
        attending.setEventDate(event.testDate());

        //TODO get current user id
        attending.setUserId("1");
        mapper.save(attending);
    }

    private void saveQrCode(Event event, String url, DynamoDBMapper mapper){
        event.setQRCodeUrl(url);
        mapper.save(event);
    }

    @Override
    protected void onPostExecute(String imagePath) {
        addPictureToGallery(imagePath);
        activity.onEventSaveSuccessNotification(eventId);
    }

    private void addPictureToGallery(String imagePath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(imagePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        activity.sendBroadcast(mediaScanIntent);
    }

    public interface NotificationInteractionListener {

    }
}

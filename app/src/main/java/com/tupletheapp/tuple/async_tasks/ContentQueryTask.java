package com.tupletheapp.tuple.async_tasks;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBQueryExpression;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.PaginatedQueryList;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.tupletheapp.tuple.custom_java.Attending;
import com.tupletheapp.tuple.custom_java.Event;
import com.tupletheapp.tuple.custom_java.EventContent;
import com.tupletheapp.tuple.utils.AmazonUtil;

import java.util.ArrayList;

public class ContentQueryTask extends AsyncTask<String, Void, ArrayList<EventContent>> {
    private Activity activity;
    private DynamoDBMapper mapper;
    private PaginatedQueryList<EventContent> result;
    private QueryListener mListener;
    private ArrayList<EventContent> futureEvents;


    private int loadLimit;

    public ContentQueryTask(Activity activity, int loadLimit, QueryListener mListener) {
        this.activity = activity;
        this.loadLimit = loadLimit;
        this.mListener = mListener;
    }

    @Override
    protected void onPreExecute() {
        mapper = new DynamoDBMapper(AmazonUtil.getDynamoDBClient(activity));
    }

    @Override
    protected ArrayList<EventContent> doInBackground(String... eventID) {
        EventContent eventContent = new EventContent();
        eventContent.setEventID(eventID[0]);

        DynamoDBQueryExpression queryExpression = new DynamoDBQueryExpression()
                .withHashKeyValues(eventContent)
                .withConsistentRead(false);

        result = mapper.query(EventContent.class, queryExpression);
        return new ArrayList<>(result);
    }

    @Override
    protected void onPostExecute(ArrayList<EventContent> events){
        if(events.isEmpty())
            mListener.onEmpty();
        else
            mListener.onComplete(events);
    }

    public interface QueryListener{
        public void onComplete(ArrayList<EventContent> eventContent);
        public void onError();
        public void onEmpty();

    }

}


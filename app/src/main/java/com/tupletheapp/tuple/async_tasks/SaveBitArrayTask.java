package com.tupletheapp.tuple.async_tasks;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.tupletheapp.tuple.activities.CameraActivity;
import com.tupletheapp.tuple.activities.CreateEventActivity;
import com.tupletheapp.tuple.utils.IOUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by ghigareda on 2/3/2016.
 */
public class SaveBitArrayTask extends AsyncTask<byte[], Void, String> {
    private Activity activity;
    private ByteArraySavedInteractionListener mListener;

    public SaveBitArrayTask(Activity activity, ByteArraySavedInteractionListener mListener){
        this.activity = activity;
        this.mListener = mListener;
    }

    @Override
    protected void onPreExecute() {
        if(activity instanceof CreateEventActivity)
            ((CreateEventActivity) activity).showDialog();
        else if (activity instanceof CameraActivity)
            ((CameraActivity) activity).showDialog();
    }

    @Override
    protected String doInBackground(byte[]... image) {
        FileOutputStream output = null;
        File mFile = IOUtil.getInstance().createImageFile();
        try {
            output = new FileOutputStream(mFile);
            output.write(image[0]);
        } catch (IOException e) {
            Log.e("ImageSaver", e.toString());
        } finally {

            if (null != output) {
                try {
                    output.close();
                } catch (IOException e) {
                    Log.e("ImageSaverIO", e.toString());
                }
            }
        }

        addPictureToGallery(mFile.getAbsolutePath());
        return mFile.getAbsolutePath();
    }

    @Override
    protected void onPostExecute(String imagePath) {
        if(activity instanceof CreateEventActivity)
            ((CreateEventActivity) activity).dismissDialog();
        else if(activity instanceof CameraActivity)
            ((CameraActivity) activity).dismissDialog();
        mListener.PhotoSaved(imagePath);
    }

    private void addPictureToGallery(String imagePath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(imagePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        activity.sendBroadcast(mediaScanIntent);
    }

    public interface ByteArraySavedInteractionListener {
        public void PhotoSaved(String coverPhotoPath);
    }
}

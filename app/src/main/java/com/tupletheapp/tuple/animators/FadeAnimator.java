package com.tupletheapp.tuple.animators;

import android.view.View;

/**
 * Created by ghigareda on 5/2/2015.
 */
public class  FadeAnimator {
    private long duration;

    public FadeAnimator(){
        duration =1500;
    }
    public void fadeIn(View label) {
        label.setAlpha(0);
        label.animate().setDuration(duration).alpha(1);
    }

    public void fadeOut(View label) {
        label.setAlpha(1);
        label.animate().setDuration(duration).alpha(0);
    }
}

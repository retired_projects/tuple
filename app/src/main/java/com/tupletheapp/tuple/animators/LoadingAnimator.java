package com.tupletheapp.tuple.animators;

import android.view.View;

import com.tupletheapp.tuple.R;

/**
 * Created by ghigareda on 5/3/2015.
 */
public class LoadingAnimator {
    private long duration;

    public LoadingAnimator(){
        duration = 1000;
    }

    public void loading(View label) {
        label.setAlpha(.1f);
    }

    public void loaded(View label) {
        label.setAlpha(.1f);
        label.animate().setDuration(duration).alpha(1);
    }
}

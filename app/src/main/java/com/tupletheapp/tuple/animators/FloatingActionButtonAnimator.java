package com.tupletheapp.tuple.animators;

import android.animation.TimeInterpolator;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;

/**
 * Created by ghigareda on 5/6/2015.
 */
public class FloatingActionButtonAnimator {
    private static final TimeInterpolator sDecelerator = new DecelerateInterpolator();
    final int ANIM_DURATION = 500;

    public FloatingActionButtonAnimator(){

    }

    public void displayFloatingActionButton(View floatingActionButton){
        floatingActionButton.setAlpha(0f);
        floatingActionButton.setScaleX(0);
        floatingActionButton.setScaleY(0);
        floatingActionButton.setRotation(1f);

        floatingActionButton.animate().setDuration(ANIM_DURATION).alpha(1f).
                scaleX(1f).scaleY(1f).rotation(0f).setInterpolator(sDecelerator);
    }

    public void hideFloatingActionButton(View floatingActionButton){
        floatingActionButton.setAlpha(1f);
        floatingActionButton.setScaleX(1);
        floatingActionButton.setScaleY(1);
        floatingActionButton.setRotation(0f);

        floatingActionButton.animate().setDuration(ANIM_DURATION).alpha(0f).
                scaleX(0f).scaleY(0f).rotation(1f).setInterpolator(sDecelerator);
    }

    public void spinFloatingActionButton(ImageButton floatingActionButton){
        floatingActionButton.setRotation(0);
        floatingActionButton.animate().setDuration(ANIM_DURATION).rotation(45).
                setInterpolator(sDecelerator);
    }

    public void unspinFloatingActionButton(ImageButton floatingActionButton){
        floatingActionButton.setRotation(45);
        floatingActionButton.animate().setDuration(ANIM_DURATION).rotation(0).
                setInterpolator(sDecelerator);
    }
}

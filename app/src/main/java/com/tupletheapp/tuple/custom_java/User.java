package com.tupletheapp.tuple.custom_java;

import java.util.ArrayList;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.*;

@DynamoDBTable(tableName = "User")
public class User {
    private String username;
    private String email;
    private String password;

    public User(){

    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String ID, String username, String email, String password) {
        this.username = username;
        this.email = email;
    }

    @DynamoDBHashKey(attributeName = "User_Email")
    public String getEmail() {
        return email;
    }

    @DynamoDBAttribute(attributeName = "Username")
    public String getUsername() {
        return username;
    }

    @DynamoDBAttribute(attributeName = "User_Password")
    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}

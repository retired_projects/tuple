package com.tupletheapp.tuple.custom_java;

import android.graphics.Bitmap;

public class NewUserInformation {
    private String username;
    private String email;
    private String password;
    private int profileColor;
    private Bitmap userProfile;

    public NewUserInformation(){

    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public int getProfileColor() {
        return profileColor;
    }

    public Bitmap getUserProfile() {
        return userProfile;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setProfileColor(int profileColor) {
        this.profileColor = profileColor;
    }

    public void setUserProfile(Bitmap userProfile) {
        this.userProfile = userProfile;
    }
}

package com.tupletheapp.tuple.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.custom_java.Event;
import com.tupletheapp.tuple.custom_views.LoadingCoverPhoto;
import com.tupletheapp.tuple.utils.NumberFormatUtil;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {
    private ArrayList<Event> eventList;
    private EventOnClickListener mListener;
    private Context context;

    public EventAdapter(Context context, ArrayList<Event> eventList, EventOnClickListener mListener) {
        this.context = context;
        this.eventList = eventList;
        this.mListener = mListener;
    }

    @Override
    public EventAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_event_card, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final EventAdapter.ViewHolder viewHolder, int i) {
        viewHolder.totalAttending.setText(
                NumberFormatUtil.getInstance().format(eventList.get(i).getTotalAttending()) + " attending"
        );
        viewHolder.location.setText(eventList.get(i).getLocationName());
        viewHolder.username.setText("Created by: Tuple");
        viewHolder.date.setText(
           eventList.get(i).getStartDate()
        );

        setCoverPhoto(viewHolder, i);
    }

    private void setCoverPhoto(final ViewHolder viewHolder, int position){
        Glide.with(context)
                .load(eventList.get(position).getCoverPhotoUrl())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        viewHolder.coverPhoto.hideProgressBar();
                        return false;
                    }
                })
                .into(viewHolder.coverPhoto.getCoverPhoto());
    }


    @Override
    public int getItemCount() {
        return eventList.size();
    }

    public void addEvents(ArrayList<Event> eventList) {
        this.eventList.addAll(eventList);
        notifyDataSetChanged();
    }

    public Event getNewestEvent() {
        return eventList.get(0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private RelativeLayout card;
        private LoadingCoverPhoto coverPhoto;
        private TextView username;
        private TextView totalAttending;
        private TextView date;
        private TextView location;

        public ViewHolder(View root) {
            super(root);
            card = (RelativeLayout) root.findViewById(R.id.card);
            coverPhoto = (LoadingCoverPhoto) root.findViewById(R.id.cover_photo);
            username = (TextView) root.findViewById(R.id.user_name);
            totalAttending = (TextView) root.findViewById(R.id.total_attending);
            date = (TextView) root.findViewById(R.id.date);
            location = (TextView) root.findViewById(R.id.location);

            card.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.card:
                    mListener.onEventSelected(eventList.get(getLayoutPosition()).getID());
                    break;
            }
        }
    }

    public interface EventOnClickListener{
        public void onEventSelected(String eventId);
    }
}
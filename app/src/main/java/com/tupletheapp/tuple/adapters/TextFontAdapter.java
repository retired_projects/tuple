package com.tupletheapp.tuple.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tupletheapp.tuple.R;

public class TextFontAdapter extends ArrayAdapter<String> {
    private Context context;
    private int resource;

    private String[] fontArray;
    private String[] fontTitleArray;

    public TextFontAdapter(Context context, int resource) {
        super(context, resource);
        this.context = context;
        this.resource = resource;
        fontArray = this.context.getResources().getStringArray(R.array.font_array);
        fontTitleArray = this.context.getResources().getStringArray(R.array.font_title_array);
    }

    ViewHolder holder;
    Drawable icon;

    class ViewHolder {
        TextView fontText;
    }

    @Override
    public View getView(int position, View convertView,
                        ViewGroup parent) {
        final LayoutInflater inflater = (LayoutInflater) context.getApplicationContext()
                .getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = inflater.inflate(resource, null);

            holder = new ViewHolder();
            holder.fontText = (TextView) convertView.findViewById(R.id.font_text);

            convertView.setTag(holder);
        } else {
            // view already defined, retrieve view holder
            holder = (ViewHolder) convertView.getTag();
        }

        holder.fontText.setText(fontTitleArray[position]);
        holder.fontText.setTypeface(Typeface.create(fontArray[position], Typeface.NORMAL));

        return convertView;
    }

    @Override
    public int getCount() {
        return fontArray.length;
    }

    public String getFont(int position) {
        return fontArray[position];
    }
}
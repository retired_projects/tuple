package com.tupletheapp.tuple.adapters;

import android.content.Context;
import android.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tupletheapp.tuple.R;

public class  FontColorAdapter extends RecyclerView.Adapter<FontColorAdapter.ViewHolder> {
    private int[] mDataset;
    private boolean selectedArray[] = {false, false, false, false, false, false, false, true};
    private int selectedColor = 7;
    private FontColorAdapterListener mListener;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        // each data item is just a string in this case
        private View color;
        private ImageView selector;
        public ViewHolder(View v) {
            super(v);
            color = v.findViewById(R.id.color_view);
            selector = (ImageView) v.findViewById(R.id.selector);
            color.setOnClickListener(this);
        }

        public void showSelector(){
            selector.setVisibility(ImageView.VISIBLE);
        }

        public void hideSelector(){
            selector.setVisibility(ImageView.GONE);
        }

        @Override
        public void onClick(View v) {
            mListener.setColor(mDataset[getPosition()]);
            notifyItemChanged(selectedColor);
            selectedArray[selectedColor] = false;
            selectedColor = getLayoutPosition();
            selectedArray[selectedColor] = true;
            notifyItemChanged(selectedColor);
        }
    }

    public FontColorAdapter(FontColorAdapterListener mListener) {
        this.mListener = mListener;
        mDataset = ((Fragment) mListener).getResources().getIntArray(R.array.font_color_array);
    }

    @Override
    public FontColorAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_color, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.color.setBackgroundColor(mDataset[position]);

        if(selectedArray[position])
            holder.showSelector();
        else
            holder.hideSelector();
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public interface FontColorAdapterListener{
        public void setColor(int color);
    }
}
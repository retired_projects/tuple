package com.tupletheapp.tuple.adapters;

import android.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.enums.FilterEnum;

public class FiltersAdapter extends RecyclerView.Adapter<FiltersAdapter.ViewHolder> {
    private String[] mDataset;
    private int[] filterPreviewImages = {
            R.drawable.normal_filter_preview,
            R.drawable.viridian_filter_preview,
            R.drawable.parallax_filter_preview,
            R.drawable.white_walls_filter_preview
    };

    private boolean selectedArray[] = {true, false, false, false};

    private FilterColorAdapter mListener;

    private int selectedFilter = 0;

    public void resetFilters(){
        selectedFilter = 0;
        selectedArray[0] = true;
        selectedArray[1] = false;
        selectedArray[2] = false;
        selectedArray[3] = false;

        for(int i = 0; i < mDataset.length; i++)
            notifyItemChanged(i);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView filterTitle;
        private ImageView filteredImage;
        private ImageView selector;

        public ViewHolder(View v) {
            super(v);
            filterTitle = (TextView) v.findViewById(R.id.filter_title);
            filteredImage = (ImageView) v.findViewById(R.id.filtered_image);
            selector = (ImageView) v.findViewById(R.id.selector);
            v.setOnClickListener(this);
        }

        public void showSelector(){
            selector.setVisibility(ImageView.VISIBLE);
        }

        public void hideSelector(){
            selector.setVisibility(ImageView.GONE);
        }

        @Override
        public void onClick(View v) {
            if(getPosition() != selectedFilter) {
                FilterEnum tempFilter;
                for(int i = 0; i < selectedArray.length; i++){
                    selectedArray[i] = false;
                    notifyItemChanged(i);
                }
                switch (getPosition()) {
                    case 0:
                        selectedFilter = 0;
                        tempFilter = FilterEnum.NORMAL;
                        break;
                    case 1:
                        selectedFilter = 1;
                        tempFilter = FilterEnum.VIRIDIAN;
                        break;
                    case 2:
                        selectedFilter = 2;
                        tempFilter = FilterEnum.PARALLAX;
                        break;
                    case 3:
                        selectedFilter = 3;
                        tempFilter = FilterEnum.WHITE_WALLS;
                        break;
                    default:
                        tempFilter = FilterEnum.NORMAL;
                        break;
                }

                selectedArray[selectedFilter] = true;
                for(int i = 0; i < 4; i++){
                    notifyItemChanged(i);
                }
                notifyItemChanged(selectedFilter);

                mListener.setFilter(tempFilter);
            }
        }
    }

    public FiltersAdapter(FilterColorAdapter mListener) {
        this.mListener = mListener;
        mDataset = ((Fragment) mListener).getResources().getStringArray(R.array.filter_titles);
    }

    @Override
    public FiltersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_filter, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.filterTitle.setText(mDataset[position]);
        holder.filteredImage.setImageResource(filterPreviewImages[position]);

        if(selectedArray[position])
            holder.showSelector();
        else
            holder.hideSelector();
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public interface FilterColorAdapter {
        public void setFilter(FilterEnum filter);
    }


}

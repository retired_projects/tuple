package com.tupletheapp.tuple.adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.activities.ContentViewActivity;
import com.tupletheapp.tuple.activities.FullScreenActivity;
import com.tupletheapp.tuple.custom_java.EventContent;

import java.util.ArrayList;
import java.util.Calendar;

public class EventContentAdapter extends RecyclerView.Adapter<EventContentAdapter.ViewHolder> {
    private final int HEADER_TAG = 0;
    private final int CONTENT_TAG = 1;
    private Activity activity;
    private ArrayList<EventContent> eventContentList;

    public EventContentAdapter(Activity activity, ArrayList<EventContent> eventContentList) {
        this.activity = activity;
        this.eventContentList = eventContentList;
    }

    @Override
    public EventContentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        switch (viewType){
            case HEADER_TAG:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_content_header, parent, false);
                break;
            case CONTENT_TAG:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_event_photo, parent, false);
                break;
        }
        return new ViewHolder(v, viewType);
    }

    @Override
    public void onBindViewHolder(final EventContentAdapter.ViewHolder holder, int position) {
        if(holder.viewType != HEADER_TAG) {
            Glide.with(activity)
                    .load(eventContentList.get(position).getUrl())
                    .into(holder.coverPhoto);

            holder.timeElapsed.setReferenceTime(Calendar.getInstance().getTimeInMillis());
            holder.name.setText("Tuple");
        }
    }

    @Override
    public int getItemCount() {
        return eventContentList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0){
           return HEADER_TAG;
        }
       return CONTENT_TAG;
    }

    public void addItem(EventContent eventContent) {
        this.eventContentList.add(1, eventContent);
        notifyDataSetChanged();
    }

    public void addItems(ArrayList<EventContent> eventContentList) {
        this.eventContentList.addAll(eventContentList);
        notifyDataSetChanged();
    }

    public void addEmptyAdapterItem(EventContent eventContent) {
        this.eventContentList.add(eventContent);
        notifyDataSetChanged();
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView name;
        private RelativeTimeTextView timeElapsed;
        private ImageView coverPhoto;
        private int viewType;

        public ViewHolder(View root, int viewType) {
            super(root);
            this.viewType = viewType;
            switch(viewType){
                case HEADER_TAG:

                    break;
                case CONTENT_TAG:
                    root.setOnClickListener(this);
                    name = (TextView) root.findViewById(R.id.name);
                    timeElapsed = (RelativeTimeTextView) root.findViewById(R.id.time_elapsed);
                    coverPhoto = (ImageView) root.findViewById(R.id.cover_photo);
                    break;
            }

        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(activity, FullScreenActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("photoUrl", eventContentList.get(getPosition()).getUrl());
            intent.putExtras(bundle);
            activity.startActivity(intent);
        }
    }
}

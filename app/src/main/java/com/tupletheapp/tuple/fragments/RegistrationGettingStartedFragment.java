package com.tupletheapp.tuple.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.activities.RegistrationActivity;
import com.tupletheapp.tuple.activities.TupleLoginActivity;

public class RegistrationGettingStartedFragment extends Fragment implements View.OnClickListener {

    private OnFragmentInteractionListener mListener;
    private Button continueButton;
    private Button existingAccountButton;
    public static RegistrationGettingStartedFragment newInstance(String param1, String param2) {
        RegistrationGettingStartedFragment fragment = new RegistrationGettingStartedFragment();
        return fragment;
    }

    public RegistrationGettingStartedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_getting_started, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        continueButton = (Button) view.findViewById(R.id.continueButton);
        continueButton.setOnClickListener(this);
        existingAccountButton = (Button) view.findViewById(R.id.existing_account_button);
        existingAccountButton.setOnClickListener(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.continueButton:
                mListener.onFragmentInteraction();
                break;
            case R.id.existing_account_button:
                Intent intent = new Intent(getActivity(), TupleLoginActivity.class);
                startActivity(intent);
        }
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction();
    }
}

package com.tupletheapp.tuple.fragments;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.tupletheapp.tuple.activities.CreateEventActivity;

public class MainMenuCreateEventFragment extends Fragment {

    private int createEventCode = 1;

    public MainMenuCreateEventFragment(){

    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startCreateActivity();
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.e("Create Event", "on resume called");
//        startCreateActivity();
    }

    private void startCreateActivity(){
        Intent myIntent = new Intent(getActivity(), CreateEventActivity.class);
        getActivity().startActivityForResult(myIntent, createEventCode);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
            getFragmentManager().popBackStackImmediate();
    }

}

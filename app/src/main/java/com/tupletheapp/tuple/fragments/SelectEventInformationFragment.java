package com.tupletheapp.tuple.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.tupletheapp.tuple.R;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class SelectEventInformationFragment extends Fragment implements View.OnClickListener {
    private SelectEventInfoOnFragmentInteractionListener mListener;

    private ImageButton backButton;
    private EditText descriptionEditText;
    private Button finishButton;

    private RelativeLayout dateContainer;
    private RelativeLayout timeContainer;
    private static TextView dateTextView;
    private static TextView timeTextView;

    private static Date date;
    private static Date time;

    private static boolean isTimeValid;
    private static boolean isDateValid;

    public static SelectEventInformationFragment newInstance() {
        SelectEventInformationFragment fragment = new SelectEventInformationFragment();
        return fragment;
    }

    public SelectEventInformationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_select_event_information, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        backButton = (ImageButton) view.findViewById(R.id.back_button);
        descriptionEditText = (EditText) view.findViewById(R.id.description);
        dateContainer = (RelativeLayout) view.findViewById(R.id.date_container);
        timeContainer = (RelativeLayout) view.findViewById(R.id.time_container);
        finishButton = (Button) view.findViewById(R.id.finish_button);
        dateTextView = (TextView) view.findViewById(R.id.date);
        timeTextView = (TextView) view.findViewById(R.id.time);

        finishButton.setEnabled(false);

        backButton.setOnClickListener(this);
        finishButton.setOnClickListener(this);
        dateContainer.setOnClickListener(this);
        timeContainer.setOnClickListener(this);
    }

    public void hideSoftKeyboard() {
        if(getActivity().getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.back_button:
                hideSoftKeyboard();
                mListener.onEventInfoBackButtonInteraction();
                break;
            case R.id.date_container:
                launchDateDialog();
                break;
            case R.id.time_container:
                launchTimeDialog();
                break;
            case R.id.finish_button:
                mListener.onFragmentInteraction(
                        descriptionEditText.getText().toString(),
                        date,
                        time
                        );
                break;
        }
    }

    private void launchDateDialog(){
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.show(getChildFragmentManager(), "datePicker");
    }

    private void launchTimeDialog(){
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getFragmentManager(), "timePicker");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (SelectEventInfoOnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface SelectEventInfoOnFragmentInteractionListener {
        public void onFragmentInteraction(String eventDescription, Date eventDate, Date eventStartTime);
        public void onEventInfoBackButtonInteraction();
    }

    public void toggleDoneButton(){
        if(isTimeValid && isDateValid)
            finishButton.setEnabled(true);
        else
            finishButton.setEnabled(false);
    }

    public class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            isDateValid = false;
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, day);
            date = calendar.getTime();
            month++;

            Calendar currentDate = Calendar.getInstance();

            if(calendar.before(currentDate)){
                isDateValid = false;
                Toast.makeText(getActivity(), "Event date must be on or after today's date", Toast.LENGTH_LONG).show();
                dateTextView.setText("Set Date");
            }else {
                dateTextView.setText((month < 10 ? "0" : "") + month + "/" + (day < 10 ? "0" : "") + day + "/" + year);
                isDateValid = true;
            }

            toggleDoneButton();
        }
    }

    public class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Calendar selectedTime = Calendar.getInstance();
            selectedTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
            selectedTime.set(Calendar.MINUTE, minute);

            time = selectedTime.getTime();

            timeTextView.setText(
                    selectedTime.get(Calendar.HOUR) + ":" + (selectedTime.get(Calendar.MINUTE) < 10 ? "0" : "") +
                            selectedTime.get(Calendar.MINUTE) + " " + (selectedTime.get(Calendar.AM_PM) == 0 ? "AM" : "PM"));

            isTimeValid = true;

            toggleDoneButton();
        }
    }
}

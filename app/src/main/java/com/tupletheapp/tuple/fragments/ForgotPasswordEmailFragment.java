package com.tupletheapp.tuple.fragments;

import android.app.Activity;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tupletheapp.tuple.R;


public class ForgotPasswordEmailFragment extends Fragment  {
    private OnFragmentInteractionListener mListener;
    private EditText userEmailEditText;
    private ProgressBar emailProgressBar;

    public static ForgotPasswordEmailFragment newInstance(String param1, String param2) {
        ForgotPasswordEmailFragment fragment = new ForgotPasswordEmailFragment();
        return fragment;
    }

    public ForgotPasswordEmailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_forgot_password_user_email, container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        emailProgressBar = (ProgressBar) view.findViewById(R.id.email_progress_bar);
        userEmailEditText = (EditText) view.findViewById(R.id.forgot_password_email_edit_text);

        setEditTextDoneButton();
        updateToolbar();
    }

    private void setEditTextDoneButton(){
        userEmailEditText.setOnKeyListener(new View.OnKeyListener(){
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER){
                    disableComponents();
                    executeEmailSearch();
                }
                return true;
            }
        });
    }

    private void executeEmailSearch(){
        //TODO: Search email on amazon service
    }

    private void updateToolbar(){
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public void onResume(){
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");
    }

    private void enableComponents(){
        userEmailEditText.setEnabled(true);
        emailProgressBar.setVisibility(View.GONE);
    }

    private void disableComponents(){
        userEmailEditText.setEnabled(false);
        emailProgressBar.setVisibility(View.VISIBLE);
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(String username, String email, String userAvatarUrl);
    }

}

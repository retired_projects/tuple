package com.tupletheapp.tuple.fragments;

import android.app.Activity;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import com.tupletheapp.tuple.R;


public class ForgotPasswordEmailSentConfirmationFragment extends Fragment implements View.OnClickListener{
    private static final String EMAIL_TAG = "email_tag";
    private static final String USERNAME_TAG = "username_tag";
    private static final String USER_AVATAR_URL_TAG = "user_avatar_url_tag";

    private EmailSentConfirmationOnFragmentInteractionListener mListener;

    private String username;
    private String userEmail;
    private String userAvatarUrl;
    private Button sendEmailButton;

    private ProgressBar emailSentProgressBar;

    private ImageView userAvatar;
    private TextView usernameTextView;
    private TextView passwordRecoveryFunText;

    public static ForgotPasswordEmailSentConfirmationFragment newInstance(String username, String email, String userAvatarUrl) {
        ForgotPasswordEmailSentConfirmationFragment fragment = new ForgotPasswordEmailSentConfirmationFragment();
        Bundle args = new Bundle();
        args.putString(USERNAME_TAG, username);
        args.putString(EMAIL_TAG, email);
        args.putString(USER_AVATAR_URL_TAG, userAvatarUrl);
        fragment.setArguments(args);
        return fragment;
    }

    public ForgotPasswordEmailSentConfirmationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            username = getArguments().getString(USERNAME_TAG);
            userEmail = getArguments().getString(EMAIL_TAG);
            userAvatarUrl = getArguments().getString(USER_AVATAR_URL_TAG);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_forgot_password_confirmation, container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        usernameTextView = (TextView) view.findViewById(R.id.username);
        passwordRecoveryFunText = (TextView) view.findViewById(R.id.email_confirmation_fun_text);
        userAvatar = (ImageView) view.findViewById(R.id.user_avatar);
        sendEmailButton = (Button) view.findViewById(R.id.send_email_button);
        emailSentProgressBar = (ProgressBar) view.findViewById(R.id.email_sent_progress_bar);

        sendEmailButton.setOnClickListener(this);
        setUserInfo();
    }

    private void setUserInfo(){
        usernameTextView.setText(username);
        passwordRecoveryFunText.setText("Hi " + username+". Hit the button below to reset your password via email");

        Glide.with(getActivity())
                .load(userAvatarUrl)
                .placeholder(R.drawable.tuple_loading_image)
                .crossFade()
                .into(userAvatar);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (EmailSentConfirmationOnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.send_email_button:
                sendEmail();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");
    }

    private void sendEmail(){
        disableComponents();
        //TODO: send email to retrieve password from Amazon service.
    }

    private void enableComponents(){
        emailSentProgressBar.setVisibility(ProgressBar.GONE);
        sendEmailButton.setEnabled(true);
    }

    private void disableComponents(){
        emailSentProgressBar.setVisibility(ProgressBar.VISIBLE);
        sendEmailButton.setEnabled(false);
    }

    public interface EmailSentConfirmationOnFragmentInteractionListener {
        public void EmailSentConfirmationOnFragmentInteraction();
    }

}

package com.tupletheapp.tuple.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.adapters.FontColorAdapter;
import com.tupletheapp.tuple.adapters.TextFontAdapter;
import com.tupletheapp.tuple.async_tasks.SavePhotoTask;
import com.tupletheapp.tuple.custom_views.EditableView;
import com.tupletheapp.tuple.decorations.ColorAdapterItemDecoration;
import com.tupletheapp.tuple.utils.BitmapUtil;

public class SelectEventNameFragment extends Fragment implements View.OnClickListener,
        SeekBar.OnSeekBarChangeListener,
        FontColorAdapter.FontColorAdapterListener,
        SavePhotoTask.PhotoSaveInteractionListener {
    private String[] editingOptionsArray = {"Font", "Size", "Color"};

    private ImageView coverPhoto;
    private EditableView editableView;

    private RelativeLayout container;
    private ImageButton fontColorButton;
    private ImageButton editModeButton;
    private ImageButton fontSizeButton;
    private ImageButton fontTypeButton;
    private ImageButton finishButton;
    private ImageButton backButton;

    private ImageButton cancelButton;
    private ImageButton acceptButton;

    private SeekBar fontSizeSeekBar;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private FontColorAdapter adapter;

    private EventInfoInteractionListener mListener;

    private String coverPhotoPath;

    private boolean isEditingModeEnabled;

    public static SelectEventNameFragment newInstance() {
        SelectEventNameFragment fragment = new SelectEventNameFragment();
        return fragment;
    }

    public SelectEventNameFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_select_event_name, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        container = (RelativeLayout) view.findViewById(R.id.container);
        coverPhoto = (ImageView) view.findViewById(R.id.event_cover_photo);
        editableView = (EditableView) view.findViewById(R.id.editable_view);
        fontSizeSeekBar = (SeekBar) view.findViewById(R.id.seekBar);

        finishButton = (ImageButton) view.findViewById(R.id.finish_button);
        fontColorButton = (ImageButton) view.findViewById(R.id.font_color);
        fontSizeButton = (ImageButton) view.findViewById(R.id.font_size);
        fontTypeButton = (ImageButton) view.findViewById(R.id.font_selection);
        backButton = (ImageButton) view.findViewById(R.id.back_button);
        editModeButton = (ImageButton) view.findViewById(R.id.editing_button);

        cancelButton = (ImageButton) view.findViewById(R.id.cancel_button);
        acceptButton = (ImageButton) view.findViewById(R.id.accept_button);

        finishButton.setOnClickListener(this);
        fontColorButton.setOnClickListener(this);
        fontSizeButton.setOnClickListener(this);
        fontTypeButton.setOnClickListener(this);
        backButton.setOnClickListener(this);
        editModeButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        acceptButton.setOnClickListener(this);

        fontSizeSeekBar.setOnSeekBarChangeListener(this);
        fontSizeSeekBar.setProgress(20);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.font_color_recycler_view);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new ColorAdapterItemDecoration(8));

        adapter = new FontColorAdapter(this);
        mRecyclerView.setAdapter(adapter);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (EventInfoInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement EventInfoInteractionListener");
        }
    }

    public void setCoverPhotoPath(String coverPhotoPath){
        this.coverPhotoPath = coverPhotoPath;
    }

    @Override
    public void onResume() {
        super.onResume();

        if(coverPhotoPath != null)
            coverPhoto.setImageBitmap(getCoverPhotoBitmap(coverPhotoPath));

        editModeButton.setClickable(true);
        fontTypeButton.setClickable(true);
        fontColorButton.setClickable(true);
        finishButton.setClickable(true);
        fontSizeButton.setClickable(true);

    }

    private Bitmap getCoverPhotoBitmap(String coverPhotoPath){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapUtil.newInstance().getRotatedBitmap(coverPhotoPath);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back_button:
                if(isEditingModeEnabled)
                    exitEditingMode();
                else
                    getActivity().onBackPressed();
                break;
            case R.id.font_selection:
                fontSizeSeekBar.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.GONE);
                showFontOptionsDialog();
                break;
            case R.id.font_size:
                fontSizeSeekBar.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
                break;
            case R.id.font_color:
                mRecyclerView.setVisibility(View.VISIBLE);
                fontSizeSeekBar.setVisibility(View.GONE);
                break;
            case R.id.finish_button:
                if(editableView.isEventNameEmpty())
                    Toast.makeText(getActivity(), "Please enter a valid event name", Toast.LENGTH_SHORT).show();
                else {
                    disableButtons();
                    saveImage();
                }
                break;
            case R.id.editing_button:
                enterEditingMode();
                break;
            case R.id.accept_button:
                exitEditingMode();
                editableView.setEditableVariables();
                break;
            case R.id.cancel_button:
                exitEditingMode();
                editableView.reset();
                break;
        }
    }

    private void disableButtons(){
        editModeButton.setClickable(false);
        fontTypeButton.setClickable(false);
        fontColorButton.setClickable(false);
        finishButton.setClickable(false);
        fontSizeButton.setClickable(false);

        exitEditingMode();
    }

    private void showFontOptionsDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose Text Font");
        final TextFontAdapter adapter = new TextFontAdapter(getActivity(), R.layout.adapter_text_font);
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                editableView.setFont(adapter.getFont(which));
            }
        });
        builder.show();
    }

    private void enterEditingMode(){
        isEditingModeEnabled = true;
        editModeButton.setVisibility(ImageButton.GONE);
        backButton.setVisibility(ImageButton.GONE);
        fontTypeButton.setVisibility(ImageButton.VISIBLE);
        fontSizeButton.setVisibility(ImageButton.VISIBLE);
        fontColorButton.setVisibility(ImageButton.VISIBLE);
        cancelButton.setVisibility(ImageButton.VISIBLE);
        acceptButton.setVisibility(ImageButton.VISIBLE);
        finishButton.setVisibility(ImageButton.GONE);
    }

    private void exitEditingMode(){
        isEditingModeEnabled = false;
        editModeButton.setVisibility(ImageButton.VISIBLE);
        backButton.setVisibility(ImageButton.VISIBLE);
        fontTypeButton.setVisibility(ImageButton.GONE);
        fontSizeButton.setVisibility(ImageButton.GONE);
        fontColorButton.setVisibility(ImageButton.GONE);
        finishButton.setVisibility(ImageButton.VISIBLE);
        cancelButton.setVisibility(ImageButton.GONE);
        acceptButton.setVisibility(ImageButton.GONE);
        fontSizeSeekBar.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        editableView.setFontSize(progress + 12);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }


    @Override
    public void setColor(int color) {
        editableView.setFontColor(color);
    }

    @Override
    public void PhotoSaved(String coverPhotoPath) {
        mListener.onEventNameOnFragmentInteraction(editableView.getEventName(), coverPhotoPath);
    }

    public interface EventInfoInteractionListener {
        public void onEventNameOnFragmentInteraction(String eventName, String coverPhotoPath);
    }

    private void saveImage(){
        container.setDrawingCacheEnabled(true);
        container.buildDrawingCache(true);
        Bitmap bitmap = Bitmap.createBitmap(container.getDrawingCache());
        container.setDrawingCacheEnabled(false);
        new SavePhotoTask(getActivity(), this).execute(bitmap);
    }
}

package com.tupletheapp.tuple.fragments;


import android.app.Activity;

import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.tupletheapp.tuple.R;

public class RegistrationCreatingProfileFragment extends Fragment implements View.OnClickListener{

    private CreatingProfileOnFragmentInteractionListener mListener;
    private ProgressBar creatingProfileProgressBar;

    public static RegistrationCreatingProfileFragment newInstance(String param1, String param2){
        RegistrationCreatingProfileFragment fragment = new RegistrationCreatingProfileFragment();
        return fragment;
    }

    public RegistrationCreatingProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_registration_creating_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        creatingProfileProgressBar = (ProgressBar) view.findViewById(R.id.creating_profile_progress_bar);
    }

    public void onAttach(Activity activity){
        super.onAttach(activity);
        try{
            mListener = (CreatingProfileOnFragmentInteractionListener) activity;
            mListener.CreatingProfileOnFragmentInteraction();
        } catch (ClassCastException e){
            throw new ClassCastException(activity.toString()
                    + " must implement onFragmentInteractionListener");
        }
    }

    public void onDetach(){
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {

    }

    public interface CreatingProfileOnFragmentInteractionListener{
        public void CreatingProfileOnFragmentInteraction();
    }

}

package com.tupletheapp.tuple.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.activities.EventActivity;
import com.tupletheapp.tuple.adapters.EventAdapter;
import com.tupletheapp.tuple.custom_java.EndlessRecyclerOnScrollListener;
import com.tupletheapp.tuple.custom_java.Event;
import com.tupletheapp.tuple.custom_views.RetryContainer;

import java.util.ArrayList;

public abstract class EventViewFragment extends Fragment
        implements EventAdapter.EventOnClickListener, RetryContainer.RetryListener {
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;

    protected ArrayList<Event> eventList;
    protected EventAdapter adapter;

    protected ProgressBar progressBar;

    protected RetryContainer retryContainer;

    private View emptyContainer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_navigation_drawer_event, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        emptyContainer = view.findViewById(R.id.empty_container);

        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        retryContainer = (RetryContainer) view.findViewById(R.id.retry_container);

        eventList = new ArrayList<>();
        adapter = new EventAdapter(getActivity(), eventList, this);
        layoutManager = new LinearLayoutManager(getActivity());

        initRecyclerView();
        loadEvents();
        retryContainer.setListener(this);
    }

    private void initRecyclerView(){
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            public void onLoadMore(int current_page, int totalItemCount) {
                loadMoreEvents(totalItemCount);
            }
        });
    }

    protected abstract void loadEvents();
    protected abstract void loadMoreEvents(int totalItemCount);

    @Override
    public void onEventSelected(String eventID) {
        Bundle bundle = new Bundle();
        bundle.putString("eventID", eventID);
        Intent intent = new Intent(getActivity(), EventActivity.class);
        intent.putExtras(bundle);
        getActivity().startActivity(intent);
    }

    @Override
    public void retryQuery() {
        retryContainer.setVisibility(RetryContainer.GONE);
        loadEvents();
    }
}

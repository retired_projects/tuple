package com.tupletheapp.tuple.fragments;

import android.app.Activity;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tupletheapp.tuple.R;


public class RegistrationPasswordFragment extends Fragment implements View.OnClickListener {

    private PasswordOnFragmentInteractionListener mListener;
    private Button continueButton;
    private EditText passwordEditText;
    private ProgressBar progressBar;

    public static RegistrationPasswordFragment newInstance(String param1, String param2) {
        RegistrationPasswordFragment fragment = new RegistrationPasswordFragment();
        return fragment;
    }

    public RegistrationPasswordFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_registration_password, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        continueButton = (Button) view.findViewById(R.id.continueButton);
        continueButton.setOnClickListener(this);
        passwordEditText=(EditText) view.findViewById(R.id.password_edit_text);
        progressBar = (ProgressBar) view.findViewById(R.id.password_progress_bar);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (PasswordOnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v){
        switch(v.getId()){
            case R.id.continueButton:
                disableComponents();
                if(!isPasswordValid(passwordEditText.getText().toString())){
                    Toast.makeText(getActivity(),"Invalid password. Please try again.",Toast.LENGTH_SHORT).show();
                    enableComponents();
                }else {
                     mListener.PasswordOnFragmentInteraction(passwordEditText.getText().toString());
                    enableComponents(); 
                }
                break;
            default:
                break;
        }
    }

    private void enableComponents() {
        continueButton.setEnabled(true);
        passwordEditText.setEnabled(true);
        progressBar.setVisibility(ProgressBar.GONE);
    }
    
    public interface PasswordOnFragmentInteractionListener {
        public void PasswordOnFragmentInteraction(String password);
    }

    private void disableComponents(){
        continueButton.setEnabled(false);
        passwordEditText.setEnabled(false);
        progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    private boolean isPasswordValid(String password){
        if(password.length() <= 8)
            return false;
        return true;
    }
}

package com.tupletheapp.tuple.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.animators.FadeAnimator;


public class ViewPagerEventMainFragment extends Fragment {
    private TextView eventName;
    private ImageView eventCoverPhoto;
    private FadeAnimator fadeAnimator;
    private boolean eventFollowed;

    private String url;

    public ViewPagerEventMainFragment() {
        fadeAnimator = new FadeAnimator();
        eventFollowed = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_view_pager_event_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setRetainInstance(true);
//        eventName = (TextView) view.findViewById(R.id.user_name);
        eventCoverPhoto = (ImageView) view.findViewById(R.id.event_cover_photo);

//        initEventInformation();
    }

    public void loadCoverPhoto(String url){
        this.url = url;

        if(eventCoverPhoto != null)
            Glide.with(getActivity())
                    .load(url)
                    .into(eventCoverPhoto);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(url != null){
            Glide.with(getActivity())
                    .load(url)
                    .into(eventCoverPhoto);
        }
    }

    private void initEventInformation() {
//        eventName.setText(getActivity().getIntent().getExtras().getString("eventName"));
        Glide.with(getActivity())
                .fromResource()
                .load(R.drawable.event_test_1)
                .into(eventCoverPhoto);
    }
}
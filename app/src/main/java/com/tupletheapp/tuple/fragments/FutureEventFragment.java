package com.tupletheapp.tuple.fragments;

import android.widget.ProgressBar;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.PaginatedQueryList;
import com.tupletheapp.tuple.async_tasks.FutureEventQueryTask;
import com.tupletheapp.tuple.custom_java.Attending;
import com.tupletheapp.tuple.custom_java.Event;
import com.tupletheapp.tuple.custom_views.RetryContainer;

import java.util.ArrayList;

public class FutureEventFragment extends EventViewFragment implements FutureEventQueryTask.OnQueryListener {
    private int loadLimit = 10;
    private int totalLoadedItems;
    private PaginatedQueryList<Attending> result;


    @Override
    protected void loadEvents() {
        new FutureEventQueryTask(getActivity(), this, loadLimit).execute();
    }

    @Override
    protected void loadMoreEvents(int totalItemCount) {

    }

    @Override
    public void setLoadLimit(int loadLimit) {
        this.loadLimit = loadLimit;
    }

    @Override
    public void setCurrentlyLoading(int currentlyLoading) {
        this.totalLoadedItems = currentlyLoading;
    }

    @Override
    public void addEvents(ArrayList<Event> events) {
        adapter.addEvents(events);
    }

    @Override
    public void displayNoResultsFound() {
        displayNoResultsFound();
    }

    @Override
    public void startLoading() {
        progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    @Override
    public void stopLoading() {
        progressBar.setVisibility(ProgressBar.GONE);
    }

    @Override
    public void onError() {
        retryContainer.setVisibility(RetryContainer.GONE);
    }
}

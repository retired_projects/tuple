package com.tupletheapp.tuple.fragments;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;

import android.net.Uri;
import android.os.Bundle;

import android.app.Fragment;
import android.app.FragmentManager;

import android.support.v13.app.FragmentStatePagerAdapter;


import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;

import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.activities.FragmentControlActivity;

import com.tupletheapp.tuple.enums.EventTypeEnum;
import com.tupletheapp.tuple.interfaces.AnimatedFragment;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.LinePageIndicator;
import com.viewpagerindicator.UnderlinePageIndicator;

import java.io.Serializable;

import javax.xml.transform.Transformer;


public class SelectEventTypeFragment extends Fragment implements
        EventTypeFragment.EventTypeOnFragmentInteractionListener {

    private ViewPager eventTypeViewPager;
    private PagerAdapter viewPagerAdapter;
    private ViewPager.PageTransformer pageTransformer;
    private EventTypeFragment eventTypeFragment;

    public CirclePageIndicator ViewPagerIndicator;

    private SelectEventTypeOnFragmentInteractionListener mListener;

    private EventTypeFragment casualFragment;
    private EventTypeFragment formalEvent;
    private EventTypeFragment sportEvent;
    private EventTypeFragment musicEvent;

    public SelectEventTypeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        casualFragment = EventTypeFragment.newInstance(
                "Casual",
                "Event",
                getResources().getString(R.string.casual_event_fun_text),
                0xFF212121,
                R.drawable.casual_event_top_image,
                R.drawable.casual_event_left_small_image,
                R.drawable.casual_event_right_top_image,
                R.drawable.casual_event_right_bottom_image,
                EventTypeEnum.CASUAL
        );
        formalEvent = EventTypeFragment.newInstance(
                "Formal",
                "Event",
                getResources().getString(R.string.formal_event_fun_text),
                0xFF9e9e9e,
                R.drawable.formal_event_top_image,
                R.drawable.formal_event_left_small_image,
                R.drawable.formal_event_right_top_image,
                R.drawable.formal_event_right_bottom_image,
                EventTypeEnum.FORMAL
                );
        sportEvent = EventTypeFragment.newInstance(
                "Sport",
                "Event",
                getResources().getString(R.string.sport_event_fun_text),
                0xFF263238,
                R.drawable.sport_event_top_image,
                R.drawable.sport_event_left_small,
                R.drawable.sport_event_right_top_image,
                R.drawable.sport_event_right_bottom_image,
                EventTypeEnum.SPORT
                );
        musicEvent = EventTypeFragment.newInstance(
                "Music",
                "Event",
                getResources().getString(R.string.music_event_fun_text),
                0xFFd32f2f,
                R.drawable.music_event_top_image,
                R.drawable.music_event_left_small,
                R.drawable.music_event_right_top_image,
                R.drawable.music_event_right_bottom_image,
                EventTypeEnum.MUSIC
                );
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_select_event_type, container, false);
    }

    public void popBackStack(){
        switch(eventTypeViewPager.getCurrentItem()){
            case 0:
                casualFragment.reverseAnimations();
                break;
            case 1:
                formalEvent.reverseAnimations();
                break;
            case 2:
                musicEvent.reverseAnimations();
                break;
            case 3:
                sportEvent.reverseAnimations();
                break;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        viewPagerAdapter = new SelectEventTypePagerAdapter(getChildFragmentManager());
        eventTypeViewPager = (ViewPager) view.findViewById(R.id.event_type_view_pager);
        ViewPagerIndicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
        pageTransformer = new DepthPageTransformer();
        setUpViewPager();
    }


    private void setUpViewPager(){
        eventTypeViewPager.setAdapter(viewPagerAdapter);
        eventTypeViewPager.setEnabled(true);
        eventTypeViewPager.setPageTransformer(false, new DepthPageTransformer());
        ViewPagerIndicator.setViewPager(eventTypeViewPager);
        eventTypeViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                AnimatedFragment fragment = (AnimatedFragment) viewPagerAdapter.instantiateItem(eventTypeViewPager, position);
                if (fragment != null)
                    fragment.fragmentShown();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    public void stopViewPager(){
        eventTypeViewPager.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                return true;
            }
        });
    }
    public void startViewPager(){
        eventTypeViewPager.setOnTouchListener(null);
    }

    public void showViewPagerIndicator(){
        ViewPagerIndicator.setVisibility(View.VISIBLE);
    }

    public void hideViewPagerIndicator(){
        ViewPagerIndicator.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (SelectEventTypeOnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void selectEventTypeFragment(EventTypeEnum eventType) {
        mListener.SelectEventTypeOnFragmentInteraction(eventType);
    }

    public interface SelectEventTypeOnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void SelectEventTypeOnFragmentInteraction(EventTypeEnum eventType);
    }

    private class SelectEventTypePagerAdapter extends FragmentStatePagerAdapter {
        private int NUM_PAGES = 4;
        public SelectEventTypePagerAdapter(FragmentManager fm){
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch(position){
                case 0:
                    return casualFragment;
                case 1:
                    return formalEvent;
                case 2:
                    return musicEvent;
                case 3:
                    return sportEvent;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    public class DepthPageTransformer implements ViewPager.PageTransformer {
        private static final float MIN_SCALE = 0.75f;

        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();

            if (position < -1)
                view.setAlpha(0);
            else if (position <= 0) {
                view.setAlpha(1);
                view.setTranslationX(0);
                view.setScaleX(1);
                view.setScaleY(1);
            } else if (position <= 1) {
                view.setAlpha(1 - position);
                view.setTranslationX(pageWidth * -position);
                float scaleFactor = MIN_SCALE + (1 - MIN_SCALE) * (1 - Math.abs(position));
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

            } else
                view.setAlpha(0);
        }
    }
}

package com.tupletheapp.tuple.fragments;

import android.app.Activity;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tupletheapp.tuple.R;


public class QrFragment extends Fragment {
    private ImageView qrCode;
    private String qrUrl;

    public QrFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_qr, container, false);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState ){
        super.onViewCreated(view, savedInstanceState);
        setRetainInstance(true);
        qrCode=(ImageView)view.findViewById(R.id.qr_image_view);
    }

    public void loadQr(String url){
        qrUrl = url;

    }

    @Override
    public void onResume() {
        super.onResume();
        if(qrUrl != null)
            Glide.with(getActivity())
                    .load(qrUrl)
                    .into(qrCode);
    }
}

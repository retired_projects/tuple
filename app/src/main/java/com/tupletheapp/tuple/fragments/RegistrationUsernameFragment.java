package com.tupletheapp.tuple.fragments;

import android.app.Activity;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tupletheapp.tuple.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegistrationUsernameFragment extends Fragment implements View.OnClickListener{
    private UsernameOnFragmentInteractionListener mListener;
    private Button continueButton;
    private EditText userNameEditText;

    private ProgressBar progressBar;

    public static RegistrationUsernameFragment newInstance(String param1, String param2) {
        RegistrationUsernameFragment fragment = new RegistrationUsernameFragment();
        return fragment;
    }

    public RegistrationUsernameFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_registration_username, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        continueButton = (Button) view.findViewById(R.id.continueButton);
        continueButton.setOnClickListener(this);
        userNameEditText=(EditText) view.findViewById(R.id.username_edit_text);
        progressBar = (ProgressBar) view.findViewById(R.id.username_progress_bar);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (UsernameOnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void onClick(View v){
        switch(v.getId()){
            case R.id.continueButton:
                disableComponents();
                if(isUserNameEmpty(userNameEditText.getText().toString()) || !isUserNameValid(userNameEditText.getText().toString())) {
                    Toast.makeText(getActivity(), "Invalid username. Please try again.", Toast.LENGTH_SHORT).show();
                    enableComponents();
                }
                else
                    checkUserName(userNameEditText.getText().toString());
                break;
            default:
                break;
        }
    }

    private void enableComponents(){
        userNameEditText.setEnabled(true);
        continueButton.setEnabled(true);
        progressBar.setVisibility(ProgressBar.GONE);
    }

    private void disableComponents(){
        userNameEditText.setEnabled(false);
        continueButton.setEnabled(false);
        progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    public interface UsernameOnFragmentInteractionListener {
        public void UsernameOnFragmentInteraction(String username);
    }
    private void checkUserName(final String username){
        //TODO: check if username has been taken on Amazon services
    }

    private boolean isUserNameValid(String username){
        boolean isValid=false;
        String expression="^[a-zA-Z0-9_.]{1,20}$";
        Pattern pattern= Pattern.compile(expression,Pattern.CASE_INSENSITIVE);
        Matcher matcher=pattern.matcher(username);
        if(matcher.matches())
           isValid=true;

        return isValid;
    }

    private boolean isUserNameEmpty(String username){
        if(username.length()>0)
            return false;
        return true;
    }
}

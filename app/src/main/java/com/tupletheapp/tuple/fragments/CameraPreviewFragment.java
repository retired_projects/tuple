package com.tupletheapp.tuple.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.hardware.Camera;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.async_tasks.SaveBitArrayTask;
import com.tupletheapp.tuple.async_tasks.SavePhotoTask;
import com.tupletheapp.tuple.custom_views.CameraPreview;
import com.tupletheapp.tuple.utils.IOUtil;

import java.io.File;


public class CameraPreviewFragment extends Fragment implements SaveBitArrayTask.ByteArraySavedInteractionListener{
    private static final String SELECTED_CAMERA = "selectedCamera";
    private static final String STATE_CAMERA_INDEX = "STATE_CAMERA_INDEX";

    private Camera camera;
    private Camera.CameraInfo cameraInfo;
    private CameraPreview cameraPreview;
    private int currentCameraIndex;
    private File imageFile;

    private CameraPreviewOnFragmentInteractionListener mListener;

    public static CameraPreviewFragment newInstance(int selectedCamera) {
        CameraPreviewFragment fragment = new CameraPreviewFragment();
        Bundle args = new Bundle();
        args.putInt(SELECTED_CAMERA, selectedCamera);
        fragment.setArguments(args);
        return fragment;
    }

    public CameraPreviewFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            currentCameraIndex = getArguments().getInt(SELECTED_CAMERA);
            imageFile = IOUtil.getInstance().createImageFile();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_camera_preview, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if(savedInstanceState != null)
            currentCameraIndex = savedInstanceState.getInt(STATE_CAMERA_INDEX);

        cameraPreview = new CameraPreview(getActivity(), currentCameraIndex == Camera.CameraInfo.CAMERA_FACING_FRONT);

        FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.camera_test);
        frameLayout.addView(cameraPreview, 0);
    }

    @Override
    public void onResume() {
        super.onResume();
        establishCamera();
    }

    private void establishCamera(){
        if(camera != null){
            cameraPreview.setCamera(null, null);
            camera.release();
            camera = null;
        }

        try{
            camera = Camera.open(currentCameraIndex);
        }catch(Exception e){
            Log.w("Log", "Could not open camera " + currentCameraIndex + " " + e.toString());
            Toast.makeText(getActivity(), "Camera couldn't start", Toast.LENGTH_SHORT).show();
            return ;
        }

        cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(currentCameraIndex, cameraInfo);
        cameraInfo.facing = currentCameraIndex;
        cameraPreview.setCamera(camera, cameraInfo);
    }

    public void turnOnFlash(){
        Camera.Parameters p = camera.getParameters();
        p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        camera.setParameters(p);
        camera.startPreview();
    }

    public void turnOffFlash(){
        Camera.Parameters p = camera.getParameters();
        p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        camera.setParameters(p);
    }

    public void takePicture(){
        camera.takePicture(null, null, pictureCallback);
    }

    Camera.PictureCallback pictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            new SaveBitArrayTask(getActivity(), CameraPreviewFragment.this).execute(data);
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        if(camera != null) {
            camera.stopPreview();
            camera.release();
            cameraPreview.setCamera(null, null);
            camera = null;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_CAMERA_INDEX, currentCameraIndex);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (CameraPreviewOnFragmentInteractionListener) getParentFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void PhotoSaved(String coverPhotoPath) {
        mListener.CameraPreviewFragmentInteraction(coverPhotoPath);
    }

    public void startProgressDialog(){

    }


    public interface CameraPreviewOnFragmentInteractionListener {
        public void CameraPreviewFragmentInteraction(String coverPhotoBitmap);
    }
}

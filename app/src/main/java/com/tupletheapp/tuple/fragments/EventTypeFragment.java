package com.tupletheapp.tuple.fragments;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.os.Handler;
import android.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.google.android.gms.maps.model.Circle;
import com.melnykov.fab.FloatingActionButton;
import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.activities.CreateEventActivity;
import com.tupletheapp.tuple.animators.FadeAnimator;
import com.tupletheapp.tuple.animators.FloatingActionButtonAnimator;
import com.tupletheapp.tuple.custom_indicators.CirclePageIndicator;
import com.tupletheapp.tuple.enums.EventTypeEnum;
import com.tupletheapp.tuple.interfaces.AnimatedFragment;

public class EventTypeFragment extends Fragment implements View.OnClickListener, AnimatedFragment{
    private EventTypeOnFragmentInteractionListener mListener;

    private static final String FIRST_TITLE_STRING_TAG= "0";
    private static final String SECOND_TITLE_STRING_TAG= "1";
    private static final String EVENT_TYPE_FUN_TEXT_TAG= "2";
    private static final String TYPE_COLOR_TAG = "3";
    private static final String BACKGROUND_REFERENCE_TAG= "4";
    private static final String LARGE_BLOCK_REFERENCE_TAG= "5";
    private static final String SMALL_BLOCK_TOP_REFERENCE_TAG= "6";
    private static final String SMALL_BLOCK_BOTTOM_REFERENCE_TAG= "7";
    private static final String EVENT_TYPE_TAG = "8";
    
    private String firstTitleString;
    private String secondTitleString;
    private String eventTypeFunTextString;
    private int typeColor;

    private int backgroundReference;
    private int largeBlockReference;
    private int smallBlockTopReference;
    private int smallBlockBottomReference;

    private ImageView background;
    private TextView funText;

    private ImageView largeBlock;
    private ImageView smallBlockTop;
    private ImageView smallBlockBottom;

    private FloatingActionButton floatingActionButton;

    private RelativeLayout eventTitleContainer;
    private TextView firstTitle;
    private TextView secondTitle;

    private AnimatorSet setAnimation;

    private View fadeInView;

    private boolean isReversed;
    private boolean animationDone;
    private FloatingActionButtonAnimator fabAnimator;
    private SelectEventTypeFragment eventTypeFragment;
    private EventTypeEnum eventType;

    public static EventTypeFragment newInstance(String firstTitleString, String secondTitleString, String eventTypeFunTextString,
            int typeColor, int backgroundReference, int largeBlockReference, int smallBlockTopReference, int smallBlockBottomReference, EventTypeEnum eventType){
        EventTypeFragment fragment = new EventTypeFragment();

        Bundle bundle = new Bundle();
        bundle.putString(FIRST_TITLE_STRING_TAG, firstTitleString);
        bundle.putString(SECOND_TITLE_STRING_TAG, secondTitleString);
        bundle.putString(EVENT_TYPE_FUN_TEXT_TAG, eventTypeFunTextString);
        bundle.putInt(TYPE_COLOR_TAG, typeColor);
        bundle.putInt(BACKGROUND_REFERENCE_TAG, backgroundReference);
        bundle.putInt(LARGE_BLOCK_REFERENCE_TAG, largeBlockReference);
        bundle.putInt(SMALL_BLOCK_TOP_REFERENCE_TAG, smallBlockTopReference);
        bundle.putInt(SMALL_BLOCK_BOTTOM_REFERENCE_TAG, smallBlockBottomReference);
        bundle.putSerializable(EVENT_TYPE_TAG, eventType);
        fragment.setArguments(bundle);

        return fragment;
    }

    public EventTypeFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null){
            firstTitleString = getArguments().getString(FIRST_TITLE_STRING_TAG);
            secondTitleString = getArguments().getString(SECOND_TITLE_STRING_TAG);
            eventTypeFunTextString = getArguments().getString(EVENT_TYPE_FUN_TEXT_TAG);
            typeColor = getArguments().getInt(TYPE_COLOR_TAG);
            backgroundReference = getArguments().getInt(BACKGROUND_REFERENCE_TAG);
            largeBlockReference = getArguments().getInt(LARGE_BLOCK_REFERENCE_TAG);
            smallBlockTopReference = getArguments().getInt(SMALL_BLOCK_TOP_REFERENCE_TAG);
            smallBlockBottomReference = getArguments().getInt(SMALL_BLOCK_BOTTOM_REFERENCE_TAG);
            eventType = (EventTypeEnum) getArguments().getSerializable(EVENT_TYPE_TAG);
        }
        isReversed = false;
        animationDone = false;
        fabAnimator = new FloatingActionButtonAnimator();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_event_type, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        background = (ImageView) view.findViewById(R.id.event_type_background);
        funText = (TextView) view.findViewById(R.id.event_type_fun_text);

        largeBlock = (ImageView) view.findViewById(R.id.event_type_large_block);
        smallBlockTop = (ImageView) view.findViewById(R.id.event_type_small_block_top);
        smallBlockBottom = (ImageView) view.findViewById(R.id.event_type_small_block_bottom);

        floatingActionButton = (FloatingActionButton) view.findViewById(R.id.fab);

        eventTitleContainer = (RelativeLayout) view.findViewById(R.id.event_title_container);
        firstTitle = (TextView) view.findViewById(R.id.event_type_first_title_text);
        secondTitle = (TextView) view.findViewById(R.id.event_type_second_title_text);

        fadeInView = view.findViewById(R.id.fade_in_view);

        floatingActionButton.show();
        floatingActionButton.setOnClickListener(this);

        setEventTypeInfo();
    }

    private void setEventTypeInfo(){
        firstTitle.setText(firstTitleString);
        secondTitle.setText(secondTitleString);
        funText.setText(eventTypeFunTextString);

        background.setImageResource(backgroundReference);
        largeBlock.setImageResource(largeBlockReference);
        smallBlockTop.setImageResource(smallBlockTopReference);
        smallBlockBottom.setImageResource(smallBlockBottomReference);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.fab:
                isReversed = false;
                fabAnimator.hideFloatingActionButton(floatingActionButton);
                ((SelectEventTypeFragment) getParentFragment()).hideViewPagerIndicator();
                ((SelectEventTypeFragment) getParentFragment()).stopViewPager();
                setTitleScaleAnimation();
                break;
        }
    }

    public void reverseAnimations(){
        isReversed = true;
        fabAnimator.displayFloatingActionButton(floatingActionButton);
        setAnimation.setInterpolator(new Interpolator() {
            @Override
            public float getInterpolation(float input) {
                return Math.abs(input - 1f);
            }
        });
        setAnimation.start();

        setAnimation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                ((SelectEventTypeFragment) getParentFragment()).showViewPagerIndicator();
                ((SelectEventTypeFragment) getParentFragment()).startViewPager();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    
    private void setTitleScaleAnimation(){
        setAnimation = new AnimatorSet();
        setAnimation.play(getScaleAnimation()).after(getColorChangeAnimation()).after(getFadeOutAnimation());
        setAnimation.start();

        setAnimation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if(!isReversed)
                    handler.postDelayed(postAnimationrRunnable, 500);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private ValueAnimator getColorChangeAnimation(){
        int startColor = ((ColorDrawable)eventTitleContainer.getBackground()).getColor();

        ValueAnimator anim = new ValueAnimator();
        anim.setIntValues(startColor, typeColor);
        anim.setEvaluator(new ArgbEvaluator());
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                eventTitleContainer.setBackgroundColor(((Integer) valueAnimator.getAnimatedValue()));
            }
        });

        anim.setDuration(300);

        return anim;
    }

    private ObjectAnimator getFadeOutAnimation(){
        PropertyValuesHolder pvhAlpha = PropertyValuesHolder.ofFloat(View.ALPHA, 1);
        ObjectAnimator fadeInAnimation = ObjectAnimator.ofPropertyValuesHolder(fadeInView, pvhAlpha);
        fadeInAnimation.setDuration(300);
        return fadeInAnimation;
    }

    private ObjectAnimator getScaleAnimation(){
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenHeight = size.y;

        int scaleFactor = (screenHeight / eventTitleContainer.getHeight()) + 1;

        PropertyValuesHolder pvhY = PropertyValuesHolder.ofFloat(View.SCALE_Y, scaleFactor);
        ObjectAnimator scaleAnimation = ObjectAnimator.ofPropertyValuesHolder(eventTitleContainer, pvhY);
        scaleAnimation.setDuration(100);
        return scaleAnimation;
    }

    private ObjectAnimator getSlideInBottomAnimation(){
        PropertyValuesHolder pvhPositionY = PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, 400, 0);
        ObjectAnimator slideInLeftAnimation = ObjectAnimator.ofPropertyValuesHolder(smallBlockBottom, pvhPositionY);
        slideInLeftAnimation.setDuration(200);
        return slideInLeftAnimation;
    }

    private ObjectAnimator getSlideInLeftAnimation(){
        PropertyValuesHolder pvhPositionY = PropertyValuesHolder.ofFloat(View.TRANSLATION_X, 400, 0);
        ObjectAnimator slideInLeftAnimation = ObjectAnimator.ofPropertyValuesHolder(smallBlockTop, pvhPositionY);
        slideInLeftAnimation.setDuration(200);
        return slideInLeftAnimation;
    }

    private ObjectAnimator getSlideInRight(){
        PropertyValuesHolder pvhPositionY = PropertyValuesHolder.ofFloat(View.TRANSLATION_X, -400, 0);
        ObjectAnimator slideInLeftAnimation = ObjectAnimator.ofPropertyValuesHolder(largeBlock, pvhPositionY);
        slideInLeftAnimation.setDuration(200);
        return slideInLeftAnimation;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (EventTypeOnFragmentInteractionListener) getParentFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void fragmentShown() {
        getSlideInBottomAnimation().start();
        getSlideInLeftAnimation().start();
        getSlideInRight().start();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface EventTypeOnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void selectEventTypeFragment(EventTypeEnum eventType);
    }

    final Handler handler = new Handler();
    Runnable postAnimationrRunnable = new Runnable() {
        public void run() {
            EventTypeFragment.this.mListener.selectEventTypeFragment(eventType);
        }
    };

}

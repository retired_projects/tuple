package com.tupletheapp.tuple.fragments;


import android.app.Activity;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.tupletheapp.tuple.R;


public class ForgotPasswordRetrievePasswordMethodFragment extends Fragment implements View.OnClickListener {

    private RetrievePasswordMethodOnFragmentInteractionListener mListener;
    private Button retrievePasswordUsingUsernameButton;
    private Button retrievePasswordUsingEmailButton;

    public static ForgotPasswordRetrievePasswordMethodFragment newInstance(String param1, String param2){
        ForgotPasswordRetrievePasswordMethodFragment fragment = new ForgotPasswordRetrievePasswordMethodFragment();
        return fragment;
    }

    public ForgotPasswordRetrievePasswordMethodFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_forgot_password_retrieve_password_method, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        retrievePasswordUsingUsernameButton = (Button) view.findViewById(R.id.select_username_method_button);
        retrievePasswordUsingUsernameButton.setOnClickListener(this);

        retrievePasswordUsingEmailButton = (Button) view.findViewById(R.id.select_email_method_button);
        retrievePasswordUsingEmailButton.setOnClickListener(this);

    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        try{
            mListener = (RetrievePasswordMethodOnFragmentInteractionListener) activity;
        }catch (ClassCastException e){
            throw new ClassCastException(activity.toString()
                    +"must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach(){
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.select_email_method_button:
                mListener.RetrievePasswordUsingEmail();
                break;

            case R.id.select_username_method_button:
                mListener.RetrievePasswordUsingUsername();
                break;
        }
    }

    public interface RetrievePasswordMethodOnFragmentInteractionListener{
        public void RetrievePasswordUsingEmail();
        public void RetrievePasswordUsingUsername();
    }
}

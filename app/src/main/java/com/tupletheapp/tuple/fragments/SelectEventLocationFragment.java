package com.tupletheapp.tuple.fragments;

import android.app.Activity;

import android.os.Bundle;

import android.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.adapters.PlaceAutocompleteAdapter;
import com.tupletheapp.tuple.custom_views.AutocompleteCustomView;
import com.tupletheapp.tuple.utils.LocationServiceUtil;

public class SelectEventLocationFragment extends GoogleConnectionFragment implements
        OnMapReadyCallback,AutoCompleteTextView.OnEditorActionListener,
        View.OnClickListener, GoogleMap.OnMyLocationButtonClickListener,
        AutocompleteCustomView.AutocompleteEventListener {

    private static final LatLngBounds DEFAULT_AUTOCOMPLETE_BOUNDS =
            new LatLngBounds(new LatLng(25.762069, -80.192457)
            ,new LatLng(49.3457868, -124.7844079));

    private final LatLng DEFAULT_MAP_LOCATION = new LatLng(31.781293, -106.442378);
    private final int DEFAULT_MAP_ZOOM_VALUE = 10;

    private PlaceAutocompleteAdapter adapter;
    private AutocompleteCustomView autocompleteCustomView;
    private LatLng eventLocationGeoPoint;
    private MapFragment mMapFragment;
    private SelectEventLocationOnFragmentInteractionListener mListener;
    public Button selectEventLocationButton;

    private GoogleMap googleMap;

    public static SelectEventLocationFragment newInstance(String param1, String param2) {
        SelectEventLocationFragment fragment = new SelectEventLocationFragment();
        return fragment;
    }

    public SelectEventLocationFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initGoogleApi();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      return inflater.inflate(R.layout.fragment_select_event_location, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        eventLocationGeoPoint = null;
        autocompleteCustomView = (AutocompleteCustomView) view.findViewById(R.id.location_auto_complete);
        autocompleteCustomView.getAutoCompleteTextView().setOnEditorActionListener(this);
        mMapFragment = MapFragment.newInstance();
        selectEventLocationButton = (Button) view.findViewById(R.id.select_location_button);
        selectEventLocationButton.setOnClickListener(this);
        selectEventLocationButton.setVisibility(View.INVISIBLE);

        autocompleteCustomView.setAutocompleteEventListener(this);

        initLocationAutocomplete();
        initLocationMap();
    }


    private void initLocationAutocomplete(){
        autocompleteCustomView.getAutoCompleteTextView().setThreshold(3);
        autocompleteCustomView.getAutoCompleteTextView().setOnItemClickListener(mAutocompleteClickListener);

        adapter = new PlaceAutocompleteAdapter(getActivity(), android.R.layout.simple_list_item_1,
                DEFAULT_AUTOCOMPLETE_BOUNDS, null);
        autocompleteCustomView.getAutoCompleteTextView().setAdapter(adapter);
    }

    private void initLocationMap(){
        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.container, mMapFragment);
        fragmentTransaction.commit();
        mMapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(DEFAULT_MAP_LOCATION, DEFAULT_MAP_ZOOM_VALUE));
        this.googleMap.getUiSettings().setScrollGesturesEnabled(false);
        this.googleMap.getUiSettings().setZoomGesturesEnabled(false);
        this.googleMap.getUiSettings().setRotateGesturesEnabled(false);
        this.googleMap.setOnMyLocationButtonClickListener(this);
        this.googleMap.setMyLocationEnabled(true);
        this.googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });
        this.googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                marker.showInfoWindow();
            }
        });
    }

    @Override
    public void onConnected(Bundle bundle) {
        adapter.setGoogleApiClient(googleApiClient);
    }

    @Override
    public void onConnectionSuspended(int i) {
        adapter.setGoogleApiClient(null);
    }


    public void hideSoftKeyboard() {
        if(getActivity().getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public boolean onEditorAction(TextView view , int actionId, KeyEvent event) {
        if(actionId==EditorInfo.IME_ACTION_DONE) {
            if(adapter.getCount() < 1 ){
                Toast.makeText(getActivity(),"Location Invalid",Toast.LENGTH_SHORT).show();
                adapter.clear();
            }else{
                setLocationResult(0);
                setTextOnResult();
                autocompleteCustomView.getAutoCompleteTextView().dismissDropDown();
            }
            SelectEventLocationFragment.this.hideSoftKeyboard();
        }
        return true;
    }

    public void setTextOnResult(){
        autocompleteCustomView.getAutoCompleteTextView().setText(adapter.getItem(0).toString());
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (SelectEventLocationOnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean onMyLocationButtonClick() {
        if(LocationServiceUtil.getInstance().isGPSEnabled() && googleMap.getMyLocation() != null) {
            googleMap.clear();
            LatLng currentLocationLatLng = new LatLng(googleMap.getMyLocation().getLatitude(), googleMap.getMyLocation().getLongitude());
            eventLocationGeoPoint = new LatLng(googleMap.getMyLocation().getLatitude(), googleMap.getMyLocation().getLongitude());
            autocompleteCustomView.getAutoCompleteTextView().setText("Current Location");
            autocompleteCustomView.showClearButton();
            autocompleteCustomView.getAutoCompleteTextView().dismissDropDown();
            panMapToLocation(currentLocationLatLng);
            placeLocationMarker(currentLocationLatLng);
            hideSoftKeyboard();
        }else
            LocationServiceUtil.getInstance().displaySettingsAlert(getActivity());
        return true;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.select_location_button:
                mListener.SelectEventLocationOnFragmentInteraction(
                        eventLocationGeoPoint,
                        autocompleteCustomView.getAutoCompleteTextView().getText().toString()
                );
                break;
        }
    }

    @Override
    public void hideSelectButton() {
        if(eventLocationGeoPoint != null) {
            final Animation in = new AlphaAnimation(1.0f, 0.0f);
            in.setDuration(500);

            selectEventLocationButton.startAnimation(in);
            in.setFillAfter(true);

            eventLocationGeoPoint = null;

            googleMap.clear();

        }
    }

    public interface SelectEventLocationOnFragmentInteractionListener {
        public void SelectEventLocationOnFragmentInteraction(LatLng location, String locationName);
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            setLocationResult(position);
            googleMap.clear();
            hideSoftKeyboard();
        }
    };

    private void setLocationResult(int position){
        final PlaceAutocompleteAdapter.PlaceAutocomplete item = adapter.getItem(position);
        final String placeId = String.valueOf(item.placeId);

        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(googleApiClient, placeId);
        placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
    }

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e("log", "Place query did not complete. Error: " + places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);

            CharSequence attributions = places.getAttributions();
            eventLocationGeoPoint = new LatLng(place.getLatLng().latitude, place.getLatLng().longitude);

            panMapToLocation(place.getLatLng());
            placeLocationMarker(place.getLatLng());
        }
    };

    private void panMapToLocation(LatLng location){
        final int CAMERA_ZOOM = 12;
        final float CAMERA_OFFSET =
                (autocompleteCustomView.getY()/(autocompleteCustomView.getY() * 10))/
                        ((CAMERA_ZOOM - DEFAULT_MAP_ZOOM_VALUE)*2);

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(location.latitude - CAMERA_OFFSET, location.longitude), CAMERA_ZOOM));
    }

    private void placeLocationMarker(LatLng location){
        Marker marker = googleMap.addMarker(new MarkerOptions()
                .position(location)
                .title(autocompleteCustomView.getAutoCompleteTextView().getText().toString()));
        marker.showInfoWindow();

        if(eventLocationGeoPoint != null) {
            final Animation in = new AlphaAnimation(0.0f, 1.0f);
            in.setDuration(3000);

            selectEventLocationButton.startAnimation(in);
            in.setFillAfter(true);
        }
    }
}

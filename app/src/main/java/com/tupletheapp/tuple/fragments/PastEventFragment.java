package com.tupletheapp.tuple.fragments;

import android.util.Log;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBQueryExpression;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.PaginatedQueryList;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.custom_java.Attending;
import com.tupletheapp.tuple.custom_java.Event;

import java.util.ArrayList;
import java.util.Calendar;

public class PastEventFragment extends EventViewFragment {

    int currentlyLoading;
    PaginatedQueryList<Attending> result;

    @Override
    protected void loadEvents() {
//        String date = "1454692115389";
//
//        Attending attending = new Attending();
//        attending.setUserId("1");
//
//        Condition rangeKeyCondition = new Condition()
//                .withComparisonOperator(ComparisonOperator.LE.toString())
//                .withAttributeValueList(new AttributeValue().withS(date));
//
//        DynamoDBQueryExpression queryExpression = new DynamoDBQueryExpression()
//                .withHashKeyValues(attending)
//                .withRangeKeyCondition("Event_Date", rangeKeyCondition)
//                .withConsistentRead(false);
//
//        result = mapper.query(Attending.class, queryExpression);
//
//        Log.e("Query", result.size() + "");
//
//        if(result.size() > 0){
//            ArrayList<Event> pastEvents = new ArrayList<>();
//
//            if(result.size() < 10)
//                loadLimit = result.size();
//
//            for (int i = 0; i < loadLimit - 1; i++){
//                Event fetchedEvent = mapper.load(Event.class, result.get(i).getEventId());
//                pastEvents.add(fetchedEvent);
//            }
//
//            adapter.addEvents(pastEvents);
//            currentlyLoading = loadLimit;
//        }
    }

    @Override
    protected void loadMoreEvents(int totalItemCount) {
//        ArrayList<Event> pastEvents = new ArrayList<>();
//
//        if(result.size() - currentlyLoading < 20)
//            loadLimit = result.size() - currentlyLoading;
//
//        for (int i = currentlyLoading; i < loadLimit - 1; i++){
//            Event fetchedEvent = mapper.load(Event.class, result.get(i).getEventId());
//            pastEvents.add(fetchedEvent);
//        }
//
//        adapter.addEvents(pastEvents);
//        currentlyLoading = loadLimit;
    }


}

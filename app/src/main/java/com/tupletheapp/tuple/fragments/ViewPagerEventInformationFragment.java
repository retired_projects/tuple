package com.tupletheapp.tuple.fragments;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;
import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.custom_java.Event;
import com.tupletheapp.tuple.utils.NumberFormatUtil;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ViewPagerEventInformationFragment extends Fragment{
    private TextView username;
    private TextView attending;
    private TextView eventDescription;
    private TextView eventDate;
    private TextView eventLocation;
    private boolean hasUserFollowed;

    public ViewPagerEventInformationFragment() {
        hasUserFollowed = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_view_pager_event_information, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setRetainInstance(true);

        username=(TextView)view.findViewById(R.id.user);
        attending=(TextView)view.findViewById(R.id.attendance);
        eventDescription=(TextView)view.findViewById(R.id.description);
        eventDate=(TextView)view.findViewById(R.id.date);
        eventLocation=(TextView)view.findViewById(R.id.location);
    }

    public void setInformation(String username,int attending, String eventDescription, String eventDate, String Location){
       this.username.setText("Tuple");
        this.attending.setText(NumberFormatUtil.getInstance().format(attending) + " attending");
        this.eventDescription.setText(eventDescription);
        this.eventLocation.setText(Location);
        this.eventDate.setText(eventDate);
    }

    private String generateStringFromDate(Date date){
        SimpleDateFormat inputFormat24 = new SimpleDateFormat("EE MMM d HH:mm:ss ZZZZ yyyy");
        SimpleDateFormat outputFormatAmPm = new SimpleDateFormat("ccc, MMMM d, yyyy");

        try {
            return outputFormatAmPm.format(inputFormat24.parse(date.toString()));
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        return "";

    }

    private String generateStringFromTime(Date date){
        SimpleDateFormat inputFormat24 = new SimpleDateFormat("EE MMM d HH:mm:ss ZZZZ yyyy");
        SimpleDateFormat outputFormatAmPm = new SimpleDateFormat("h:mm a");

        try {
            return outputFormatAmPm.format(inputFormat24.parse(date.toString()));
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        return "";
    }

    private String generateStringFromGeoPoint(LatLng latLng){
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(addresses != null)
            return addresses.get(0).getAddressLine(0) + " " + addresses.get(0).getLocality() + " " + addresses.get(0).getAdminArea() + "-" + addresses.get(0).getPostalCode();
        else
            return "";
    }
}

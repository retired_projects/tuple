package com.tupletheapp.tuple.fragments;


import android.app.Activity;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tupletheapp.tuple.R;


public class ForgotPasswordUsernameFragment extends Fragment {

    private UserUsernameOnFragmentInteractionListener mListener;
    private EditText userUsernameEditText;
    private ProgressBar usernameProgressBar;


    public static ForgotPasswordUsernameFragment newInstance(String param1, String param2){
        ForgotPasswordUsernameFragment fragment = new ForgotPasswordUsernameFragment();
        return fragment;
    }

    public ForgotPasswordUsernameFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_forgot_password_user_username, container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        usernameProgressBar = (ProgressBar)view.findViewById(R.id.username_progress_bar);
        userUsernameEditText = (EditText) view.findViewById(R.id.forgot_password_username_edit_text);

        setEditTextDoneButton();
        updateToolbar();
    }

    private void setEditTextDoneButton(){
        userUsernameEditText.setOnKeyListener(new View.OnKeyListener(){
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER){
                    disableComponents();
                    executeUsernameSearch();
                }
                return true;
            }
        });
    }

    private void updateToolbar(){
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");
    }

    private void executeUsernameSearch(){
        //TODO: Search username on Amazon service
    }

    private void enableComponents(){
        userUsernameEditText.setEnabled(true);
        usernameProgressBar.setVisibility(View.GONE);
    }

    private void disableComponents(){
        userUsernameEditText.setEnabled(false);
        usernameProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
    try{
        mListener = (UserUsernameOnFragmentInteractionListener) activity;
    }catch (ClassCastException e){
        throw new ClassCastException(activity.toString()
                +" must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach(){
        super.onDetach();
        mListener = null;
    }

    public interface UserUsernameOnFragmentInteractionListener{
        public void UserUsernameOnFragmentInteractionListener(String username, String email, String userAvatarUrl);
    }
}

package com.tupletheapp.tuple.fragments;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.activities.CreateEventActivity;
import com.tupletheapp.tuple.adapters.FiltersAdapter;
import com.tupletheapp.tuple.async_tasks.SavePhotoTask;
import com.tupletheapp.tuple.enums.FilterEnum;
import com.tupletheapp.tuple.interfaces.AnimatedFragment;
import com.tupletheapp.tuple.utils.BitmapUtil;
import com.tupletheapp.tuple.utils.FilterUtil;
import com.tupletheapp.tuple.utils.IOUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

public class SelectEventPhotoPreviewFragment extends Fragment implements  View.OnClickListener,
        FiltersAdapter.FilterColorAdapter,
        SavePhotoTask.PhotoSaveInteractionListener {
    private SelectEventPhotoPreviewOnFragmentInteractionListener mListener;

    private View buttonContainer;
    private String coverPhotoPath;
    private ImageView coverPhoto;

    private Button okButton;
    private Button cancelButton;

    private AnimatorSet showMenuAnimationSet;
    private AnimatorSet hideMenuAnimationSet;

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private FiltersAdapter adapter;

    private boolean isMenuShown;

    private GestureDetectorCompat mDetector;

    private Bitmap originalBitmap;
    private FilterEnum currentFilter = FilterEnum.NORMAL;

    ImageLoader imageLoader = ImageLoader.getInstance();

    public static SelectEventPhotoPreviewFragment newInstance() {
        SelectEventPhotoPreviewFragment fragment = new SelectEventPhotoPreviewFragment();
        return fragment;
    }

    public SelectEventPhotoPreviewFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_select_photo_preview, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        mDetector = new GestureDetectorCompat(getActivity(), new ImageGestureListener());
        isMenuShown = true;

        buttonContainer = view.findViewById(R.id.button_container);
        coverPhoto = (ImageView) view.findViewById(R.id.event_cover_photo);

        okButton = (Button) view.findViewById(R.id.ok_button);
        cancelButton = (Button) view.findViewById(R.id.cancel_button);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new FiltersAdapter(this);
        recyclerView.setAdapter(adapter);

        setCoverPhotoTouchListener();

        cancelButton.setOnClickListener(this);
        okButton.setOnClickListener(this);
    }

    public void resetFilter(){
        currentFilter = FilterEnum.NORMAL;
        adapter.resetFilters();
        recyclerView.smoothScrollToPosition(0);
        layoutManager.scrollToPosition(0);
    }

    private void setCoverPhotoTouchListener(){
        coverPhoto.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mDetector.onTouchEvent(event);
                return true;
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (SelectEventPhotoPreviewOnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setCoverPhotoPath(final String coverPhotoPath){
        this.coverPhotoPath = coverPhotoPath;

    }


    @Override
    public void onResume() {
        playShowMenuAnimations();

        if(coverPhotoPath!=null)
            imageLoader.loadImage("file:///"+coverPhotoPath, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    originalBitmap = BitmapUtil.newInstance().getRotatedBitmap(loadedImage, coverPhotoPath);
                    coverPhoto.setImageBitmap(originalBitmap);
                }
            });
        super.onResume();
    }

    @Override
    public void onPause() {
        playHideMenuAnimations();
        super.onPause();
    }

    public void resetCoverPhoto(){
        coverPhoto.setImageBitmap(null);
    }

    private void playShowMenuAnimations(){
        showMenuAnimationSet = new AnimatorSet();
        showMenuAnimationSet.playTogether(getYAnimation(buttonContainer, -400, 0), getYAnimation(recyclerView, 1000, 0));
        showMenuAnimationSet.start();
    }

    private void playHideMenuAnimations(){
        hideMenuAnimationSet = new AnimatorSet();
        hideMenuAnimationSet.playTogether(getYAnimation(buttonContainer, 0, -400), getYAnimation(recyclerView, 0, 1000));
        hideMenuAnimationSet.start();
    }

    private ObjectAnimator getYAnimation(View animatedView, int startingY, int endingY){
        PropertyValuesHolder pvhPositionY = PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, startingY,endingY);
        ObjectAnimator slideInLeftAnimation = ObjectAnimator.ofPropertyValuesHolder(animatedView, pvhPositionY);
        slideInLeftAnimation.setDuration(500);
        return slideInLeftAnimation;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ok_button:
                saveImage();
                break;
            case R.id.cancel_button:
                getActivity().onBackPressed();
                break;
        }
    }

    private void saveImage(){
        new SavePhotoTask(getActivity(), this).execute(((BitmapDrawable)coverPhoto.getDrawable()).getBitmap());
    }

    @Override
    public void setFilter(FilterEnum filter) {
        currentFilter = filter;
        new ApplyFilterTask().execute(currentFilter);
    }

    @Override
    public void PhotoSaved(String coverPhotoPath) {
        mListener.SelectEventPhotoPreviewOnFragmentInteraction(coverPhotoPath);
    }

    public interface SelectEventPhotoPreviewOnFragmentInteractionListener {
        public void SelectEventPhotoPreviewOnFragmentInteraction(String coverPhotoPath);
    }

    class ImageGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDown(MotionEvent event) {
            return true;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            if(isMenuShown) {
                playHideMenuAnimations();
                isMenuShown = false;
            }else {
                playShowMenuAnimations();
                isMenuShown = true;
            }
            return true;
        }
    }

    private class ApplyFilterTask extends AsyncTask<FilterEnum, Void, Bitmap> {
        protected Bitmap doInBackground(FilterEnum... filter) {
            switch (filter[0]){
                case NORMAL:
                    return originalBitmap;
                case VIRIDIAN:
                    return FilterUtil.getInstance().applyViridianFilter(getActivity(), originalBitmap);
                case WHITE_WALLS:
                    return FilterUtil.getInstance().applyWhiteWallsFilter(getActivity(), originalBitmap);
                case PARALLAX:
                    return FilterUtil.getInstance().applyParallaxFilter(getActivity(), originalBitmap);
                default:
                    return originalBitmap;
            }
        }

        protected void onPostExecute(Bitmap result) {
            coverPhoto.setImageBitmap(result);
        }
    }
}

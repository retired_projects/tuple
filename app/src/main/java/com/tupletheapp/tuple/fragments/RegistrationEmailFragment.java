package com.tupletheapp.tuple.fragments;

import android.app.Activity;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.async_tasks.CheckEmailTask;
import com.tupletheapp.tuple.custom_java.User;
import com.tupletheapp.tuple.utils.AmazonUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class RegistrationEmailFragment extends Fragment implements View.OnClickListener {
    private EmailOnFragmentInteractionListener mListener;
    private Button continueButton;
    private EditText emailEditText;
    private ProgressBar progressBar;


    public static RegistrationEmailFragment newInstance(String param1, String param2) {
        RegistrationEmailFragment fragment = new RegistrationEmailFragment();
        return fragment;
    }

    public RegistrationEmailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_registration_email, container, false);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        continueButton = (Button) view.findViewById(R.id.continueButton);
        continueButton.setOnClickListener(this);
        emailEditText = (EditText) view.findViewById(R.id.registration_email);
        progressBar = (ProgressBar) view.findViewById(R.id.email_progress_bar);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (EmailOnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v){
        switch(v.getId()){
            case R.id.continueButton:
                disableComponents();

                if(isEmailEmpty(emailEditText.getText().toString()) || !isEmailValid(emailEditText.getText().toString())){
                    enableComponents();
                    Toast.makeText(getActivity(), "Invalid email. Please try again.", Toast.LENGTH_SHORT).show();
                }else
                   checkEmailAvailability(emailEditText.getText().toString());
                break;
            default:
                break;
        }
    }

    private void checkEmailAvailability(final String email){
        new CheckEmailTask(getActivity()).execute(email);
    }

    private void enableComponents(){
        emailEditText.setEnabled(true);
        continueButton.setEnabled(true);
        progressBar.setVisibility(ProgressBar.GONE);
    }

    private void disableComponents(){
        emailEditText.setEnabled(false);
        continueButton.setEnabled(false);
        progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    public interface EmailOnFragmentInteractionListener {
        public void emailOnFragmentInteraction(String email);
    }

    private static boolean isEmailValid(String email) {
        boolean isValid = false;
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);

        if (matcher.matches())
            isValid = true;

        return isValid;
    }

    private boolean isEmailEmpty(String email){
        if(email.length()>0)
            return false;
        return true;
    }
}

package com.tupletheapp.tuple.fragments;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CaptureRequest;
import android.media.MediaActionSound;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;

import com.tupletheapp.tuple.R;

import java.io.Serializable;


public class SelectEventCoverPhotoFragment extends PhotoGalleryFragment implements View.OnClickListener,  CameraPreviewFragment.CameraPreviewOnFragmentInteractionListener {
    private SelectCoverPhotoOnFragmentInteractionListener mListener;

    private CameraPreviewFragment rearCameraFragment;
    private CameraPreviewFragment frontCameraFragment;

    private ImageButton takePhotoButton;
    private ImageButton switchCameraButton;
    private ImageButton openPhotoLibraryButton;
    private ImageButton flashButton;

    private boolean isFrontCamera;
    private boolean isFlashOn;

    public SelectEventCoverPhotoFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null)
            initialSetUp();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_select_event_cover_photo, container, false);
    }

    private void initialSetUp(){
        isFrontCamera = false;

        rearCameraFragment = CameraPreviewFragment.newInstance(Camera.CameraInfo.CAMERA_FACING_BACK);
        frontCameraFragment = CameraPreviewFragment.newInstance(Camera.CameraInfo.CAMERA_FACING_FRONT);

        getChildFragmentManager()
                .beginTransaction()
                .add(R.id.container, rearCameraFragment)
                .commit();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        takePhotoButton = (ImageButton) view.findViewById(R.id.take_photo_button);
        switchCameraButton = (ImageButton) view.findViewById(R.id.switch_camera_button);
        openPhotoLibraryButton = (ImageButton) view.findViewById(R.id.open_photo_library_button);
        flashButton = (ImageButton) view.findViewById(R.id.flash_button);

        takePhotoButton.setOnClickListener(this);
        switchCameraButton.setOnClickListener(this);
        openPhotoLibraryButton.setOnClickListener(this);
        flashButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.take_photo_button:
                playCameraShutter();
                hideComponents();


                if(isFrontCamera) {
                    frontCameraFragment.takePicture();
                    frontCameraFragment.startProgressDialog();
                }else {
                    rearCameraFragment.takePicture();
                    rearCameraFragment.startProgressDialog();
                }

                break;
            case R.id.switch_camera_button:
                flipCamera();
                break;
            case R.id.open_photo_library_button:
                startGalleryIntent();
                break;
            case R.id.flash_button:
                toggleFlash();
                break;
        }
    }

    private void toggleFlash(){
        if(isFlashOn){
            turnOffFlash();
        }else{
            turnOnFlash();
        }
    }

    public void restartCamera(){
        if(isFrontCamera)
            frontCameraFragment.onResume();
        else
            rearCameraFragment.onResume();
    }

    private void turnOnFlash(){
        isFlashOn = true;
        flashButton.setImageResource(R.drawable.flash_off);
        rearCameraFragment.turnOnFlash();
    }

    public void turnOffFlash(){
        isFlashOn = false;
        flashButton.setImageResource(R.drawable.flash_on);
        rearCameraFragment.turnOffFlash();
    }

    private void hideComponents(){
        takePhotoButton.setVisibility(Button.GONE);
        switchCameraButton.setVisibility(Button.GONE);
        openPhotoLibraryButton.setVisibility(Button.GONE);
        flashButton.setVisibility(Button.GONE);
    }

    public void showComponents(){
        takePhotoButton.setVisibility(Button.VISIBLE);
        switchCameraButton.setVisibility(Button.VISIBLE);
        openPhotoLibraryButton.setVisibility(Button.VISIBLE);
        isFlashOn = false;
        flashButton.setVisibility(Button.VISIBLE);
        restartCamera();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (SelectCoverPhotoOnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == GALLERY_INTENT_TAG && resultCode == getActivity().RESULT_OK){
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(
                    selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String filePath = cursor.getString(columnIndex);
            cursor.close();

            mListener.SelectCoverPhotoOnFragmentInteraction(filePath);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void playCameraShutter(){
        MediaActionSound sound = new MediaActionSound();
        sound.play(MediaActionSound.SHUTTER_CLICK);
    }

    private void flipCamera() {
        if (isFrontCamera) {
            getChildFragmentManager().popBackStack();
            isFrontCamera = false;
            flashButton.setVisibility(ImageButton.VISIBLE);
            return;
        }

        isFrontCamera= true;
        turnOffFlash();

        flashButton.setVisibility(ImageButton.GONE);

        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.container, frontCameraFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void CameraPreviewFragmentInteraction(String coverPhoto) {
        mListener.SelectCoverPhotoOnFragmentInteraction(coverPhoto);
    }

    public interface SelectCoverPhotoOnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void SelectCoverPhotoOnFragmentInteraction(String coverPhoto);
    }
}

package com.tupletheapp.tuple.callbacks;

import com.tupletheapp.tuple.enums.ScrollState;

public interface ObservableScrollViewCallbacks {
    void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging);
    void onDownMotionEvent();
    void onUpOrCancelMotionEvent(ScrollState scrollState);
}
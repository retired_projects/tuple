package com.tupletheapp.tuple.enums;

public enum ScrollState {
    STOP,
    UP,
    DOWN,
}
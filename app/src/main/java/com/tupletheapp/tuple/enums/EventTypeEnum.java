package com.tupletheapp.tuple.enums;


public enum EventTypeEnum {
    CASUAL,
    FORMAL,
    MUSIC,
    SPORT
}

package com.tupletheapp.tuple.enums;

/**
 * Created by ghigareda on 12/14/2015.
 */
public enum FilterEnum {
    NORMAL,
    WHITE_WALLS,
    VIRIDIAN,
    PARALLAX
}

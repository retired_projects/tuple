package com.tupletheapp.tuple.Dialog;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tupletheapp.tuple.R;

/**
 * Created by ghigareda on 2/3/2016.
 */
public class ImageProgressDialog extends DialogFragment {

    public static ImageProgressDialog newInstance() {
        return new ImageProgressDialog();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Translucent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.progress_dialog_image, container, false);
    }

}

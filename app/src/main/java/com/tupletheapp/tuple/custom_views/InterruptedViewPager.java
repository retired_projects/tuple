package com.tupletheapp.tuple.custom_views;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

import com.bumptech.glide.GenericTranscodeRequest;
import com.tupletheapp.tuple.fragments.SelectEventTypeFragment;

public class InterruptedViewPager extends ViewPager{
    boolean isScroll = false;
    boolean isClick = false;

    public InterruptedViewPager(Context context) {
        super(context);
        mGestureDetector = new GestureDetector(context, new XScrollDetector());
    }

    public InterruptedViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        mGestureDetector = new GestureDetector(context, new XScrollDetector());
    }

    private GestureDetector mGestureDetector;

    private class XScrollDetector extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            isScroll = true;
            isClick = false;
            return true;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            isScroll = false;
            isClick = false;
            return true;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            isScroll = false;
            isClick = true;
            return true;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        mGestureDetector.onTouchEvent(ev);
        return true;
    }
}

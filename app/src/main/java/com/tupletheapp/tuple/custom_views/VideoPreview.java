package com.tupletheapp.tuple.custom_views;

import android.content.Context;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.List;

/**
 * Created by ghigareda on 2/1/2016.
 */
public class VideoPreview extends SurfaceView implements SurfaceHolder.Callback {
    private MediaRecorder recorder;
    private final SurfaceHolder surfaceHolder;

    private boolean isRecording;

    public VideoPreview(Context context){
        super(context);
        recorder = new MediaRecorder();
        initRecorder();
        surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);
    }

    public void initRecorder(){
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);

        CamcorderProfile cpHigh = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
        recorder.setProfile(cpHigh);
        recorder.setOutputFile("/sdcard/videocapture_example.mp4");
        recorder.setMaxDuration(10000); // 50 seconds
        recorder.setMaxFileSize(5000000); // Approximately 5 megabytes
    }

    private void prepareRecorder() {
        recorder.setPreviewDisplay(surfaceHolder.getSurface());

        try {
            recorder.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        recorder.start();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        prepareRecorder();

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (isRecording) {
            recorder.stop();
            isRecording = false;
        }
        recorder.release();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        widthMeasureSpec = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        heightMeasureSpec = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
    }

}


package com.tupletheapp.tuple.custom_views;

import android.content.Context;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.List;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private final SurfaceHolder surfaceHolder;
    private Camera camera;
    private Camera.CameraInfo cameraInfo;
    private boolean isSurfaceCreated;
    private boolean isFrontCamera;

    public CameraPreview(Context context, boolean isFrontCamera){
        super(context);
        isSurfaceCreated = false;
        surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        this.isFrontCamera = isFrontCamera;
    }


    public void setCamera(Camera camera, Camera.CameraInfo cameraInfo){
        if(this.camera != null){
            try{
                this.camera.stopPreview();
            } catch(Exception ignored){}
        }

        this.camera = camera;
        this.cameraInfo = cameraInfo;

        if(!isSurfaceCreated){
            return;
        }

        try{
            configureCamera();
            camera.setPreviewDisplay(surfaceHolder);
            camera.startPreview();
        }catch (Exception e){

        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        isSurfaceCreated = true;

        if(camera!=null)
            setCamera(camera, cameraInfo);

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {


    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if(camera == null || surfaceHolder.getSurface() == null)
            return;
        try{
            camera.stopPreview();
        }catch(Exception e){

        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        widthMeasureSpec = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        heightMeasureSpec = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
    }

    private void configureCamera(){
        Camera.Parameters parameters = camera.getParameters();

        Camera.Size targetPreviewSize = getClosestSize(getWidth(), getHeight(), parameters.getSupportedPreviewSizes());
        parameters.setPreviewSize(targetPreviewSize.width, targetPreviewSize.height);

        Camera.Size targetImageSize = getClosestSize(getWidth(), getHeight(), parameters.getSupportedPictureSizes());
        parameters.setPictureSize(targetImageSize.width, targetImageSize.height);

        if(!isFrontCamera)
            parameters.setRotation(90);
        else
            parameters.setRotation(270);

        camera.setDisplayOrientation(90);
        camera.setParameters(parameters);

    }

    private Camera.Size getClosestSize(int width, int height, List<Camera.Size> supportedSizes){
        final double ASPECT_TOLERANCE = .1;

        double targetRatio = (double) height/width;

        Camera.Size targetSize = null;

        double minDifference = Double.MAX_VALUE;

        for(Camera.Size size : supportedSizes){
            double ratio = (double) size.width / size.height;
            if(Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;

            int heightDifference = Math.abs(size.height - height);
            if(heightDifference < minDifference){
                targetSize = size;
                minDifference = heightDifference;
            }
        }

        if(targetSize == null){
            minDifference = Double.MAX_VALUE;
            for(Camera.Size size : supportedSizes){
                int heightDifference = Math.abs(size.height - height);
                if(heightDifference < minDifference){
                    targetSize = size;
                    minDifference = heightDifference;
                }

            }
        }

        return targetSize;
    }
}

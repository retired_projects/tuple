package com.tupletheapp.tuple.custom_views;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tupletheapp.tuple.R;

/**
 * Created by ghigareda on 2/7/2016.
 */
public class LoadingCoverPhoto extends FrameLayout {
    private ImageView coverPhoto;
    private ProgressBar progressBar;

    public LoadingCoverPhoto(Context context) {
        super(context);
        init(context);
    }

    public LoadingCoverPhoto(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context){
        View root = inflate(context, R.layout.cover_photo_loading_image, this);
        coverPhoto = (ImageView) root.findViewById(R.id.image_view);
        progressBar = (ProgressBar) root.findViewById(R.id.progress_bar);
    }

    public void showProgressBar(){
        progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    public void hideProgressBar(){
        progressBar.setVisibility(ProgressBar.GONE);
    }

    public ImageView getCoverPhoto(){
        return coverPhoto;
    }

}

package com.tupletheapp.tuple.custom_views;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.adapters.PlaceAutocompleteAdapter;

public class AutocompleteCustomView extends FrameLayout implements View.OnClickListener, TextWatcher{
    private AutoCompleteTextView autoCompleteTextView;
    private ImageButton clearButton;
    private AutocompleteEventListener mListener;

    public AutocompleteCustomView(Context context) {
        super(context);
        init(context);
    }

    public AutocompleteCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context){
        View root = inflate(context, R.layout.custom_view_auto_complete, this);
        autoCompleteTextView = (AutoCompleteTextView) root.findViewById(R.id.auto_complete);
        clearButton = (ImageButton) root.findViewById(R.id.clear_text_button);
        clearButton.setOnClickListener(this);
        autoCompleteTextView.addTextChangedListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.clear_text_button:
                clearText();
                hideClearButton();
                break;
        }
    }

    private void clearText(){
        autoCompleteTextView.setText("");
        ((PlaceAutocompleteAdapter)autoCompleteTextView.getAdapter()).clear();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if(before == 0 && count == 1)
            showClearButton();

        if(before == 1 && count == 0 && autoCompleteTextView.getText().length() < 1) {
            hideClearButton();
            ((PlaceAutocompleteAdapter)autoCompleteTextView.getAdapter()).clear();
        }
    }

    public void showClearButton(){
        clearButton.setVisibility(Button.VISIBLE);
    }

    private void hideClearButton(){
        clearButton.setVisibility(Button.GONE);
        if(mListener!=null)
            mListener.hideSelectButton();
    }

    @Override
    public void afterTextChanged(Editable s) {
        if(s.toString().isEmpty()){
            hideClearButton();
            ((PlaceAutocompleteAdapter) autoCompleteTextView.getAdapter()).clear();
        }
    }

    public void setAdapter(){

    }

    public AutoCompleteTextView getAutoCompleteTextView(){
        return autoCompleteTextView;
    }


    public interface AutocompleteEventListener{
        public void hideSelectButton();
    }

    public void setAutocompleteEventListener(AutocompleteEventListener mListener){
        this.mListener = mListener;
    }
}

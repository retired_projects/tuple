package com.tupletheapp.tuple.custom_views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Vibrator;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tupletheapp.tuple.R;

public class EditableView extends FrameLayout implements TextView.OnEditorActionListener {
    private View background;
    private EditText editText;
    private TextView textView;

    private int color = Color.WHITE;
    private int size = 32;
    private int updatedSize;
    private String startingFont = "sans-serif-medium";
    private String updatedFont;

    private GestureDetectorCompat mDetector;

    public EditableView(Context context) {
        super(context);
        init(context);
    }

    public EditableView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context){
        View root = inflate(context, R.layout.editable_view, this);
        background = root.findViewById(R.id.container);
        editText = (EditText) root.findViewById(R.id.edit_text);
        textView = (TextView)root.findViewById(R.id.text_view);

        background.setPadding(0, getOffset(), 0, 0);

        textView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return mDetector.onTouchEvent(event);
            }
        });

        editText.setOnEditorActionListener(this);
        mDetector = new GestureDetectorCompat(context, new MyGestureListener());
    }

    public void setEditableVariables(){
        color = textView.getCurrentTextColor();
        size = updatedSize;
        startingFont = updatedFont;
    }

    public void reset(){
        setFontSize(size);
        setFontColor(color);
        setFont(startingFont);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
            setTextViewText();
            editText.setVisibility(EditText.GONE);
            textView.setVisibility(EditText.VISIBLE);
            dismissKeyboard();
            return true;
        }
        return false;
    }

    private void setTextViewText(){
        if(editText.getText().toString().isEmpty())
            textView.setText("Event Name");
        else
            textView.setText(editText.getText());
    }

    private void showKeyboard(){
        editText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    private void dismissKeyboard(){
        InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public void setFontSize(int fontSize){
        updatedSize = fontSize;
        editText.setTextSize(fontSize);
        textView.setTextSize(fontSize);
    }

    public void setFont(String fontTypeFace){
        updatedFont = fontTypeFace;
        editText.setTypeface(Typeface.create(fontTypeFace, Typeface.NORMAL));
        textView.setTypeface(Typeface.create(fontTypeFace, Typeface.NORMAL));
    }

    public void setFontStyle(int fontStyle){
        editText.setTypeface(null, fontStyle);
        textView.setTypeface(null, fontStyle);
    }

    public void setFontColor(int textColor){
        editText.setTextColor(textColor);
        textView.setTextColor(textColor);
    }

    public boolean isEventNameEmpty(){
        return editText.getText().toString().isEmpty();
    }

    public String getEventName(){
        return editText.getText().toString();
    }


    public class MyGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDown(MotionEvent event) {
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            int startPadding = 0;
            int topPadding = getOffset();

            if(e2.getRawX() - (textView.getWidth()/2) > 0)
                startPadding = (int) e2.getRawX() - (textView.getWidth()/2);

            if(e2.getRawY() > getOffset()) {
                topPadding = (int) e2.getRawY();

                if(e2.getRawY() + textView.getHeight() > getSizeOfScreen() - getOffset())
                    topPadding = getSizeOfScreen() - getOffset() - textView.getHeight();
            }


            background.setPadding(startPadding,topPadding, 0, 0);

            return true;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
                editText.setVisibility(EditText.VISIBLE);
                textView.setVisibility(EditText.GONE);
                editText.requestFocus();
                showKeyboard();
            return true;
        }

    }

    public int getOffset(){
        TypedValue tv = new TypedValue();
        if (getContext().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            return TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }else
            return 224;
    }

    public int getSizeOfScreen(){
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point screenSize = new Point();
        display.getSize(screenSize);
        return screenSize.y;
    }
}
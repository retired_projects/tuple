package com.tupletheapp.tuple.custom_views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.tupletheapp.tuple.R;

public class RetryContainer extends RelativeLayout implements View.OnClickListener{
    private Button retryButton;
    private RetryListener mListener;

    public RetryContainer(Context context) {
        super(context);
        init(context);
    }

    public RetryContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context){
        View root = inflate(context, R.layout.retry_container, this);
        retryButton = (Button) root.findViewById(R.id.retry_button);
        retryButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.retry_button:
                mListener.retryQuery();
                break;
        }
    }

    public void setListener(RetryListener mListener){
        this.mListener = mListener;
    }

    public interface RetryListener{
        public void retryQuery();
    }
}

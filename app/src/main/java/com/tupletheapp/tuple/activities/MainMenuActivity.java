package com.tupletheapp.tuple.activities;

import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentManager;

import android.os.StrictMode;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.custom_views.NoneSwipeableViewPager;
import com.tupletheapp.tuple.fragments.FutureEventFragment;

import com.tupletheapp.tuple.fragments.PastEventFragment;
import com.tupletheapp.tuple.utils.AmazonUtil;

public class MainMenuActivity extends ActionBarActivity implements View.OnClickListener{
    private final int CREATE_EVENT_REQUEST_CODE = 1;
    private final int QR_SCAN_REQUEST_CODE = 2;

    private Toolbar toolbar;

    private NoneSwipeableViewPager viewPager;
    private RadioButton futureEventButton;
    private RadioButton followEventButton;
    private RadioButton pastEventButton;

    private FutureEventFragment futureEventsFragment;
    private PastEventFragment pastEventsFragment;

    private DynamoDBMapper mapper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //TODO Delete after async tasks are added
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.activity_main_menu);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        viewPager = (NoneSwipeableViewPager) findViewById(R.id.viewpager);
        futureEventButton = (RadioButton) findViewById(R.id.future_event_button);
        followEventButton = (RadioButton) findViewById(R.id.follow_event_button);
        pastEventButton = (RadioButton) findViewById(R.id.past_event_button);

        futureEventsFragment = new FutureEventFragment();
        pastEventsFragment = new PastEventFragment();

        setSupportActionBar(toolbar);
        futureEventButton.setChecked(true);
        setupViewPager(viewPager);

        futureEventButton.setOnClickListener(this);
        followEventButton.setOnClickListener(this);
        pastEventButton.setOnClickListener(this);

        mapper = new DynamoDBMapper(AmazonUtil.getDynamoDBClient(this));
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.future_event_button:
                viewPager.setCurrentItem(0, true);
//                testDB();
                break;
            case R.id.follow_event_button:
               startQRScannerActivity();
                break;
            case R.id.past_event_button:
                viewPager.setCurrentItem(1, true);
                break;
        }
    }

    public void startQRScannerActivity(){
        Intent scannerActivity = new Intent(this, QRScanner.class);
        startActivityForResult(scannerActivity, QR_SCAN_REQUEST_CODE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
            super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_create_event:
                startActivityForResult(new Intent(this, CreateEventActivity.class),CREATE_EVENT_REQUEST_CODE);
                overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_bottom);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

//    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//        if (requestCode == QR_SCAN_REQUEST_CODE){
//          //TODO load newly followed event
//        }
//    }

    private void setupViewPager(ViewPager viewPager) {
        viewPager.setAdapter(new ViewPagerAdapter(getFragmentManager()));
    }


    public class ViewPagerAdapter extends FragmentStatePagerAdapter {

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            switch(position){
                case 0:
                    return futureEventsFragment;
                case 1:
                    return pastEventsFragment;
                default:
                    return futureEventsFragment;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "";
        }
    }
}

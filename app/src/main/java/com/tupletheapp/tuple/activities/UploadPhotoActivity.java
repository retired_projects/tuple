package com.tupletheapp.tuple.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.bumptech.glide.Glide;
import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.utils.AmazonUtil;
import com.tupletheapp.tuple.utils.MediaUtil;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class UploadPhotoActivity extends ActionBarActivity {
    private final int CAMERA_INTENT_TAG = 0;
    private final int GALLERY_INTENT_TAG = 1;
    private final String[] UPLOAD_PHOTO_OPTIONS = {"Take photo", "Choose from gallery"};

    private Toolbar toolbar;

    private AlphaAnimation inAnimation;
    private AlphaAnimation outAnimation;

    private View progressBarContainer;

    private RecyclerView recyclerView;
    private SelectedImageAdapter adapter;
    private String currentPhotoPath;
    private Bitmap photo;
    private String path;

    private TransferUtility transferUtility;
    private String s3path;
    private String s3pathThumbnail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (getIntent().getExtras().getInt("type") == CAMERA_INTENT_TAG) {
            startCameraIntent();
        } else {
            startGalleryIntent();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_photo);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        transferUtility = AmazonUtil.getTransferUtility(this);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressBarContainer = findViewById(R.id.progress_bar_container);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new SelectedImageAdapter();
        recyclerView.setAdapter(adapter);
    }

    private void displayProgressBar() {
        inAnimation = new AlphaAnimation(0f, 1f);
        inAnimation.setDuration(200);
        progressBarContainer.setAnimation(inAnimation);
        progressBarContainer.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(RecyclerView.GONE);
        getSupportActionBar().hide();
    }

    private void hideProgressBar() {
        Toast.makeText(this, "Photo upload failed: Please try again", Toast.LENGTH_LONG).show();
        outAnimation = new AlphaAnimation(1f, 0f);
        outAnimation.setDuration(200);
        progressBarContainer.setAnimation(outAnimation);
        recyclerView.setVisibility(RecyclerView.VISIBLE);
        getSupportActionBar().show();
        progressBarContainer.setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_photo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done:
                displayProgressBar();
                saveSelectedImagesToParse();
                break;
            case android.R.id.home:
                if (arePhotosSelected())
                    confirmBack();
                else
                    finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void startCameraIntent() {
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(captureIntent, CAMERA_INTENT_TAG);
    }

    private void startGalleryIntent() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, GALLERY_INTENT_TAG);
    }


    private void saveSelectedImagesToParse() {
        saveToS3(path);
    }

    private void sendResult(String eventContentObjectId) {
        Intent intent = new Intent();

        SelectedImageAdapter.ViewHolder viewHolder = ((SelectedImageAdapter.ViewHolder) recyclerView.getChildViewHolder(recyclerView.getChildAt(0)));
//        String caption = viewHolder.captionFloatingLabelEditText.getEditText().getText().toString();

        Bundle bundle = new Bundle();
        bundle.putString("selectedPhoto", currentPhotoPath);
//        bundle.putString("comment", caption.trim().isEmpty() ? "" : caption);
        bundle.putString("eventContentObjectId", eventContentObjectId);
        intent.putExtras(bundle);

        setResult(RESULT_OK, intent);
        finish();
    }

    private void saveToS3(String filePath) {
//        s3path = ParseUser.getCurrentUser().getUsername() + "/_event_photo_" +
//                Calendar.getInstance().getTime().toString().replace(" ", "_") + ".jpg";
//        photo = MediaUtil.getInstance().resizeBitmap(photo);
//        File file = MediaUtil.getInstance().bitmapToFile(photo, "content");
//        transferUtility.upload(getResources().getString(R.string.bucket_name), s3path, file);
//
//        s3pathThumbnail = ParseUser.getCurrentUser().getUsername() + "/_event_photo_" +
//                Calendar.getInstance().getTime().toString().replace(" ", "_") + "_thumbnail.jpg";
//        File thumbnailFile = MediaUtil.getInstance().bitmapToFile(MediaUtil.getInstance().generateContentThumbnail(photo), "thumbnail");
//        transferUtility.upload(getResources().getString(R.string.bucket_name), s3pathThumbnail, thumbnailFile);
    }


    private byte[] generateBitmapArray(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);

        if (bitmap.getByteCount() > 200000) {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        }

        return stream.toByteArray();
    }

    public void confirmBack() {
        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.upload_photo_cancel_title))
                .setMessage(getResources().getString(R.string.upload_photo_cancel_message))
                .setPositiveButton(getResources().getString(R.string.upload_comment_cancel_positive), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.upload_comment_cancel_negative), null)
                .show();
    }

    @Override
    public void onBackPressed() {
        if (progressBarContainer.getVisibility() == View.GONE) {
            if (arePhotosSelected())
                confirmBack();
            else
                super.onBackPressed();
        } else {
            Toast.makeText(this, "Uploading photo to event, please give us a minute", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if ((requestCode == CAMERA_INTENT_TAG || requestCode == GALLERY_INTENT_TAG)) {
                Uri uri = data.getData();
                path = MediaUtil.getInstance().getPath(uri, this);
                photo = MediaUtil.getInstance().getBitmap(resultCode, uri, this, path);
                adapter.addCameraPhoto(uri);
            } else
                finish();
        }
    }

    private boolean arePhotosSelected() {
        boolean arePhotosSelected = false;
        if (!adapter.isSelectedPhotosEmpty())
            arePhotosSelected = true;
        return arePhotosSelected;
    }

    private void initProgressBar(int position) {
        View child = recyclerView.getChildAt(position);
        SelectedImageAdapter.ViewHolder holder = (SelectedImageAdapter.ViewHolder) recyclerView.getChildViewHolder(child);
        holder.progressBar.setVisibility(ProgressBar.VISIBLE);
        holder.progressBar.setProgress(0);
    }

    private void setPhotoUploadProgress(int position, int progress) {
        View child = recyclerView.getChildAt(position);
        SelectedImageAdapter.ViewHolder holder = (SelectedImageAdapter.ViewHolder) recyclerView.getChildViewHolder(child);
        holder.progressBar.setProgress(progress);
    }

    private void dismissRecyclerViewChild(int position) {
        adapter.removeAt(position);
    }

    public class SelectedImageAdapter extends RecyclerView.Adapter<SelectedImageAdapter.ViewHolder> {
        private ArrayList<Uri> selectedImagesUriList;
        private Bitmap selectedImageBitmap;

        public SelectedImageAdapter() {
            selectedImagesUriList = new ArrayList<>();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView selectedImage;
            private ProgressBar progressBar;
//            private FloatingLabelEditText captionFloatingLabelEditText;

            public ViewHolder(View root) {
                super(root);
                selectedImage = (ImageView) root.findViewById(R.id.photo_upload);
                progressBar = (ProgressBar) root.findViewById(R.id.progress_bar);
//                captionFloatingLabelEditText = (FloatingLabelEditText) root.findViewById(R.id.photo_caption);
            }
        }

        @Override
        public SelectedImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_photo_upload, parent, false);

            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final SelectedImageAdapter.ViewHolder holder, int position) {
            if (selectedImagesUriList.size() > 1)
                Glide.with(UploadPhotoActivity.this)
                        .load(selectedImagesUriList.get(position).getPath())
                        .error(R.drawable.tuple_logo)
                        .into(holder.selectedImage);
            else {
                setPic(holder.selectedImage, selectedImagesUriList.get(position).getPath());
            }

            holder.progressBar.setVisibility(ProgressBar.GONE);
        }

        private void setPic(ImageView imageView, String uri) {
            imageView.setImageBitmap(photo);
        }

        @Override
        public int getItemCount() {
            return selectedImagesUriList.size();
        }

        public void addSelectedImagesUris(ArrayList<Uri> selectedImagesUriList) {
            this.selectedImagesUriList.addAll(selectedImagesUriList);
            notifyDataSetChanged();
        }

        public void addCameraPhoto(Uri uri) {
            selectedImagesUriList.add(uri);
            notifyDataSetChanged();
        }

        public boolean isSelectedPhotosEmpty() {
            return selectedImagesUriList.isEmpty();
        }

        public ArrayList<Uri> getSelectedPhotos() {
            return selectedImagesUriList;
        }

        public void removeAt(int position) {
            selectedImagesUriList.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, selectedImagesUriList.size());
        }
    }
}

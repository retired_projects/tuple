package com.tupletheapp.tuple.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tupletheapp.tuple.R;

public class TupleLoginActivity extends FragmentActivity implements View.OnClickListener {
    private Button signUpButton;
    private Button forgotPasswordButton;
    private Button loginButton;

    private EditText loginUsername;
    private EditText loginPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if(isUserLoggedIn()){
            startMainMenu();
        }

        signUpButton = (Button) findViewById(R.id.signUpButton);
        signUpButton.setOnClickListener(this);

        forgotPasswordButton = (Button) findViewById(R.id.forgot_password_button);
        forgotPasswordButton.setOnClickListener(this);

        loginPassword=(EditText)findViewById(R.id.login_password_edit_text);
        loginUsername=(EditText)findViewById(R.id.login_username_edit_text);
        loginButton = (Button) findViewById(R.id.login_button);
        loginButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.signUpButton:
                startSignUpActivity();
                break;
            case R.id.forgot_password_button:
                startForgotPasswordActivity();
                break;
            case R.id.login_button:
                validateLoginInfo();
                break;
        }
    }

    public void startSignUpActivity(){
        Intent signUpActivity = new Intent(this, RegistrationActivity.class);
        this.startActivity(signUpActivity);
    }

    public void startForgotPasswordActivity(){
        Intent forgotPasswordActivity = new Intent(this, ForgotPasswordActivity.class);
        this.startActivity(forgotPasswordActivity);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if(isUserLoggedIn()){
            Intent mainMenuIntent = new Intent(this, MainMenuActivity.class);

            if(getIntent().getExtras() != null)
                mainMenuIntent.putExtras(intent.getExtras());

            mainMenuIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            mainMenuIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mainMenuIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            this.startActivity(mainMenuIntent);
        }
    }

    private void startMainMenu(){
        Intent intent = new Intent(this, SplashPageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        this.startActivity(intent);
    }


    private boolean isUserLoggedIn(){
        return true;
    }

    private void loginUser(){
        //TODO: Implement login user using Amazon services
    }

    private void validateLoginInfo(){
        if(loginUsername.getText().toString().isEmpty()){
            Toast.makeText(this, "Username cannot be empty", Toast.LENGTH_LONG).show();
            return;
        } if(loginPassword.getText().toString().isEmpty()){
            Toast.makeText(this, "Password cannot be empty", Toast.LENGTH_LONG).show();
            return;
        }
        loginUser();
    }

}

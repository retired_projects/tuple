package com.tupletheapp.tuple.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.amazonaws.services.s3.AmazonS3Client;
import com.bumptech.glide.Glide;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.nineoldandroids.view.ViewHelper;
import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.adapters.EventContentAdapter;
import com.tupletheapp.tuple.animators.FadeAnimator;
import com.tupletheapp.tuple.async_tasks.EventInformationQueryTask;
import com.tupletheapp.tuple.async_tasks.ContentQueryTask;
import com.tupletheapp.tuple.callbacks.ObservableScrollViewCallbacks;
import com.tupletheapp.tuple.custom_indicators.CirclePageIndicator;
import com.tupletheapp.tuple.custom_java.EndlessRecyclerOnScrollListener;
import com.tupletheapp.tuple.custom_java.Event;
import com.tupletheapp.tuple.custom_java.EventContent;
import com.tupletheapp.tuple.custom_views.ObservableRecyclerView;
import com.tupletheapp.tuple.decorations.EventActivityRecyclerViewItemDecoration;
import com.tupletheapp.tuple.enums.ScrollState;
import com.tupletheapp.tuple.fragments.QrFragment;
import com.tupletheapp.tuple.fragments.ViewPagerEventInformationFragment;
import com.tupletheapp.tuple.fragments.ViewPagerEventMainFragment;
import com.tupletheapp.tuple.utils.AmazonUtil;
import com.tupletheapp.tuple.utils.MediaUtil;
import com.tupletheapp.tuple.utils.ScrollUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EventActivity extends ActionBarActivity implements ObservableScrollViewCallbacks, View.OnClickListener,
        ContentQueryTask.QueryListener, EventInformationQueryTask.QueryListener {
    private final int CAMERA_INTENT_TAG = 0;
    private final int GALLERY_INTENT_TAG = 1;
    private final String[] CONTENT_OPTIONS = {"Take Photo", "Record Video"};
    private final int QUERY_LIMIT = 8;
    private Toolbar toolbar;
    private ViewPager viewPager;
    private EventInfoViewPagerAdapter viewPagerAdapter;
    private CirclePageIndicator titleIndicator;
    private View emptyContainer;
    private View mOverlayView;

    private int mFlexibleSpaceImageHeight;
    private FadeAnimator fadeAnimator;
    private ImageButton uploadPhotoActionButton;
    private ProgressBar progressBar;
    private ObservableRecyclerView recyclerView;
    private EventContentAdapter adapter;
    private GridLayoutManager layoutManager;
    private boolean contentInEvent;

    private ArrayList<EventContent> eventContentList;

    private ViewPagerEventInformationFragment viewPagerEventInformationFragment;
    private ViewPagerEventMainFragment viewPagerEventMainFragment;
    private QrFragment qrFragment;

    private AmazonS3Client s3Client;

    private String eventID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        eventContentList = new ArrayList<>();

        setContentView(R.layout.activity_event);
        eventID = getIntent().getExtras().getString("eventID");
        mFlexibleSpaceImageHeight = getResources().getDimensionPixelSize(R.dimen.flexible_space_image_height);
        contentInEvent = false;
        fadeAnimator = new FadeAnimator();
        viewPagerEventInformationFragment = new ViewPagerEventInformationFragment();
        viewPagerEventMainFragment = new ViewPagerEventMainFragment();
        qrFragment= new QrFragment();

        initToolbar();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mOverlayView = findViewById(R.id.overlay);
        emptyContainer = findViewById(R.id.empty_container);

        uploadPhotoActionButton = (ImageButton) findViewById(R.id.add_event_content_button);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        fadeAnimator = new FadeAnimator();

        s3Client = AmazonUtil.getS3Client(this);

        initFloatingActionButtons();
        initEventGrid();
        initEmptyContainer();
        initViewPager();

        ViewHelper.setTranslationY(mOverlayView, mFlexibleSpaceImageHeight);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initFloatingActionButtons() {
        uploadPhotoActionButton = (ImageButton) findViewById(R.id.add_event_content_button);
        uploadPhotoActionButton.setOnClickListener(this);
        uploadPhotoActionButton.setScaleX(0);
        uploadPhotoActionButton.setScaleY(0);
        uploadPhotoActionButton.animate().scaleX(1).setDuration(500);
        uploadPhotoActionButton.animate().scaleY(1).setDuration(500);
    }

    private void initViewPager() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        viewPagerAdapter = new EventInfoViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);

        titleIndicator = (CirclePageIndicator) findViewById(R.id.titles);
        titleIndicator.setViewPager(viewPager);
    }


    private void initEventGrid() {
        recyclerView = (ObservableRecyclerView) findViewById(R.id.recycler_view);
        layoutManager = new GridLayoutManager(this, 1);
        recyclerView.setScrollViewCallbacks(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(false);
        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            public void onLoadMore(int current_page, int totalItemCount) {
//                loadMoreEventContent(event, totalItemCount);
            }
        });


        adapter = new EventContentAdapter(this, eventContentList);
        adapter.addEmptyAdapterItem(new EventContent());
        recyclerView.setAdapter(adapter);

        loadEvent();
    }


    private void initEmptyContainer() {
        TextView emptyContainerTitle = (TextView) emptyContainer.findViewById(R.id.title);
        TextView emptyContainerBottomLabel = (TextView) emptyContainer.findViewById(R.id.bottom_label);

        emptyContainerTitle.setText(getResources().getString(R.string.event_activity_empty_container_title));
        emptyContainerBottomLabel.setText(getResources().getString(R.string.event_activity_empty_container_label));

        emptyContainer.setVisibility(View.GONE);
    }

    private void loadEvent() {
        new EventInformationQueryTask(this,this).execute(eventID);
        loadEventContent();
    }

    private void loadEventContent() {
       new ContentQueryTask(this, 10, this).execute(eventID);
    }

    private void loadMoreEventContent( int totalItemCount) {
        //TODO: Load event content using Amazon
    }

    private void displayEventContent(ArrayList<EventContent> eventContentList) {
        recyclerView.setVisibility(RecyclerView.VISIBLE);
        adapter.addItems(eventContentList);
        fadeAnimator.fadeIn(recyclerView);
    }

    private void displayMoreEventContent(ArrayList<EventContent> eventContentList) {
        adapter.addItems(eventContentList);
    }

    private void displayEmptyEventContentMessage() {
        emptyContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_event_content_button:
                launchUploadPhotoDialog();
                break;
        }
    }

    private void launchUploadPhotoDialog() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle("Upload Content")
                .setItems(CONTENT_OPTIONS, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0)
                            addContent(new Intent(EventActivity.this, CameraActivity.class),
                                    getResources().getInteger(R.integer.photo_request_code), CAMERA_INTENT_TAG);
                        else
                            addContent(new Intent(EventActivity.this, UploadPhotoActivity.class),
                                    getResources().getInteger(R.integer.photo_request_code), GALLERY_INTENT_TAG);
                    }
                });
        builder.show();
    }

    private void addContent(Intent intent, int requestCode, int type) {
        Bundle bundle = new Bundle();
        bundle.putString("eventID", eventID);
        intent.putExtras(bundle);
        startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == getResources().getInteger(R.integer.photo_request_code) && resultCode == RESULT_OK) {
            if (data != null) {
                handleResult(new EventContent(
                        data.getExtras().getString("contentID"),
                        data.getExtras().getString("contentCreatorID"),
                        data.getExtras().getString("contentEventID"),
                        data.getExtras().getBoolean("contentType"),
                        data.getExtras().getString("contentUrl")
                ));
            }
        }
    }

    private void handleResult(EventContent eventContent) {
        adapter.addItem(eventContent);
    }


    protected int getActionBarSize() {
        TypedValue typedValue = new TypedValue();
        int[] textSizeAttr = new int[]{R.attr.actionBarSize};
        int indexOfAttrTextSize = 0;
        TypedArray a = obtainStyledAttributes(typedValue.data, textSizeAttr);
        int actionBarSize = a.getDimensionPixelSize(indexOfAttrTextSize, -1);
        a.recycle();
        return actionBarSize;
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        int minOverlayTransitionY = 0 - mOverlayView.getHeight();
        ViewHelper.setTranslationY(mOverlayView, ScrollUtils.getFloat(-scrollY, minOverlayTransitionY, 0));
        ViewHelper.setTranslationY(viewPager, ScrollUtils.getFloat(-scrollY, minOverlayTransitionY, 0));
        ViewHelper.setTranslationY(titleIndicator, ScrollUtils.getFloat(-scrollY, minOverlayTransitionY, 0));
//        ViewHelper.setTranslationY(mRecyclerViewBackground, Math.max(0, -scrollY + mFlexibleSpaceImageHeight));
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }

    @Override
    public void onComplete(ArrayList<EventContent> eventContent) {
        adapter.addItems(eventContent);
    }

    @Override
    public void onError() {

    }

    @Override
    public void onEmpty() {
        displayEmptyEventContentMessage();
    }

    @Override
    public void onComplete(Event event) {
        viewPagerEventMainFragment.loadCoverPhoto(event.getCoverPhotoUrl());
        viewPagerEventInformationFragment.setInformation(event.getUserId(),event.getTotalAttending(),event.getEventDescription(),
                event.getStartDate(),event.getLocationName());
        qrFragment.loadQr(event.getQRCodeUrl());
    }

    private class EventInfoViewPagerAdapter extends FragmentStatePagerAdapter {
        public EventInfoViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return viewPagerEventMainFragment;
                case 1:
                    return viewPagerEventInformationFragment;
                case 2:
                    return qrFragment;
                default:
                    return null;
            }

        }

        @Override
        public int getCount() {
            return 3;
        }
    }


}

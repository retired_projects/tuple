package com.tupletheapp.tuple.activities;

import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.zxing.qrcode.QRCodeWriter;
import com.tupletheapp.tuple.Dialog.ImageProgressDialog;
import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.async_tasks.AmazonSaveTask;
import com.tupletheapp.tuple.custom_java.Event;
import com.tupletheapp.tuple.enums.EventTypeEnum;
import com.tupletheapp.tuple.fragments.SelectEventInformationFragment;
import com.tupletheapp.tuple.fragments.SelectEventNameFragment;
import com.tupletheapp.tuple.interfaces.MultipleFragmentActivity;
import com.tupletheapp.tuple.fragments.SelectEventCoverPhotoFragment;
import com.tupletheapp.tuple.fragments.SelectEventPhotoPreviewFragment;
import com.tupletheapp.tuple.fragments.SelectEventLocationFragment;
import com.tupletheapp.tuple.fragments.SelectEventTypeFragment;
import com.tupletheapp.tuple.utils.BitmapUtil;

import java.net.URI;
import java.util.Calendar;
import java.util.Date;


public class CreateEventActivity extends FragmentControlActivity
        implements MultipleFragmentActivity,
        SelectEventCoverPhotoFragment.SelectCoverPhotoOnFragmentInteractionListener,
        SelectEventLocationFragment.SelectEventLocationOnFragmentInteractionListener,
        SelectEventPhotoPreviewFragment.SelectEventPhotoPreviewOnFragmentInteractionListener,
        SelectEventNameFragment.EventInfoInteractionListener,
        SelectEventInformationFragment.SelectEventInfoOnFragmentInteractionListener {

    private SelectEventCoverPhotoFragment eventCoverPhotoFragment;
    private SelectEventLocationFragment eventLocationFragment;
    private SelectEventPhotoPreviewFragment eventPhotoPreviewFragment;
    private SelectEventNameFragment eventNameFragment;
    private SelectEventInformationFragment eventInformationFragment;

    private FragmentShownState currentFragmentState;

    private NotificationCompat.Builder mBuilder;
    private NotificationManager mNotificationManager;

    private Event event;

    private ImageProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null){
            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.cancel(1);
            retryUpload(bundle);
        } else{
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

            setContentView(R.layout.activity_create_event);
            eventCoverPhotoFragment = new SelectEventCoverPhotoFragment();
            eventLocationFragment = new SelectEventLocationFragment();
            eventPhotoPreviewFragment = new SelectEventPhotoPreviewFragment();
            eventPhotoPreviewFragment = SelectEventPhotoPreviewFragment.newInstance();
            eventInformationFragment = SelectEventInformationFragment.newInstance();
            eventNameFragment = new SelectEventNameFragment();
            currentFragmentState = FragmentShownState.EVENT_LOCATION;
            event = new Event();
            dialog = ImageProgressDialog.newInstance();
            dialog.setCancelable(false);

            fragmentSetup();
        }
    }


    @Override
    public void fragmentSetup() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.create_event_container, eventLocationFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    @Override
    public void onBackPressed() {
        FragmentManager fm = getFragmentManager();
        if (currentFragmentState != FragmentShownState.EVENT_LOCATION) {
            if (fm.getBackStackEntryCount() > 1) {
                    fm.popBackStack();
                    if (currentFragmentState == FragmentShownState.EVENT_NAME || currentFragmentState == FragmentShownState.PHOTO_PREVIEW) {
                        eventCoverPhotoFragment.showComponents();
                    }
                    if (currentFragmentState == FragmentShownState.PHOTO_PREVIEW) {
                        eventPhotoPreviewFragment.resetFilter();
                        eventPhotoPreviewFragment.resetCoverPhoto();
                    }
            }
        } else
            finish();

        setStateAfterStackPop();
    }

    private void setStateAfterStackPop() {
        switch (currentFragmentState) {
            case EVENT_COVER_PHOTO:
                currentFragmentState = FragmentShownState.EVENT_LOCATION;
                break;
            case PHOTO_PREVIEW:
                currentFragmentState = FragmentShownState.EVENT_COVER_PHOTO;
                break;
            case EVENT_NAME:
                currentFragmentState = FragmentShownState.PHOTO_PREVIEW;
                break;
            case EVENT_INFO:
                currentFragmentState = FragmentShownState.EVENT_NAME;
            default:
                break;
        }
    }


    @Override
    public void SelectEventLocationOnFragmentInteraction(LatLng location, String locationName) {
        currentFragmentState = FragmentShownState.EVENT_COVER_PHOTO;

        event.setLocationCoordinates(location.latitude + "," + location.longitude);
        event.setLocationName(locationName);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.create_event_container, eventCoverPhotoFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void SelectCoverPhotoOnFragmentInteraction(String coverPhoto) {
        currentFragmentState = FragmentShownState.PHOTO_PREVIEW;
        eventPhotoPreviewFragment.setCoverPhotoPath(coverPhoto);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.create_event_container, eventPhotoPreviewFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void SelectEventPhotoPreviewOnFragmentInteraction(String coverPhotoPath) {
        currentFragmentState = FragmentShownState.EVENT_NAME;
        eventNameFragment.setCoverPhotoPath(coverPhotoPath);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.create_event_container, eventNameFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onEventNameOnFragmentInteraction(String eventName, String coverPhotoPath) {
        currentFragmentState = FragmentShownState.EVENT_INFO;

        event.setEventName(eventName);
        event.setCoverPhotoPath(coverPhotoPath);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.create_event_container, eventInformationFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onFragmentInteraction(String eventDescription, Date eventDate, Date eventStartTime) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, eventDate.getHours());
        cal.set(Calendar.MINUTE, eventDate.getMinutes());
        cal.set(Calendar.MONTH, eventDate.getMonth());
        cal.set(Calendar.DAY_OF_MONTH, eventDate.getDate());
        cal.set(Calendar.YEAR, eventDate.getYear());

        event.setEventDescription(eventDescription);
        long date = 1454692115490l;
        event.setEventStartDate(date);
        startNotification();
    }

    public void showDialog(){
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(R.id.create_event_container, dialog);
        transaction.show(dialog);
        transaction.commit();
    }

    public void dismissDialog(){
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.remove(dialog);
        transaction.commit();
    }

    @Override
    public void onEventInfoBackButtonInteraction() {
        onBackPressed();
    }

    private void startNotification() {
        Toast.makeText(this, "Uploading event. Check notifications for progress", Toast.LENGTH_LONG).show();

        mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setContentTitle("Saving Event " + "\" Event Name \"")
                .setContentText("Uploading cover photo")
                .setSmallIcon(R.drawable.ic_wb_cloudy_white_24dp)
                .setLargeIcon(BitmapUtil.newInstance().getRotatedBitmap(event.getCoverPhotoPath()))
                .setProgress(0, 0, true);

        mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(1, mBuilder.build());


        new AmazonSaveTask(this).execute(event);
        finish();
    }

//    @Override
//    public void onFileSaveSuccessInteraction() {
//        mBuilder.setContentTitle("Saving Event " + "Event Name")
//                .setContentText("Saving event");
//    }


    public void onEventSaveSuccessNotification(String eventId) {
        mBuilder.setContentTitle("Event has been saved")
                .setContentText("Click to view")
                .setContentIntent(createSuccessIntent(eventId))
                .setSmallIcon(R.drawable.ic_done_white_24dp)
                .setProgress(0, 0, false);

        mNotificationManager.notify(
                1,
                mBuilder.build());

    }

    private PendingIntent createSuccessIntent(String eventId) {
        Intent successIntent = new Intent(this, EventActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("eventID", eventId);
        successIntent.putExtras(bundle);

        PendingIntent successPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        successIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        return successPendingIntent;
    }

    private void retryUpload(Bundle bundle){
        Log.e("Retry", "Recreating Event");

//        event = new Event(this);
//
//        event.setEventName(bundle.getString("Name"));
//        event.setEventDescription(bundle.getString("Description"));
//        event.setCoverPhotoPath(bundle.getString("Photo Path"));

        Double longitude = bundle.getDouble("Longitude");
        Double latitude = bundle.getDouble("Latitude");

//        //TODO get event location name
//        event.setEventLocation(location, "Location");
//
//        Log.e("Location", "" + event.getEventLocation().getLongitude());
//        event.setEventType(getTye(bundle.getInt("Type")));

        Long startDate = bundle.getLong("Start Date");
        Long startTime = bundle.getLong("Start Time");

        Date sDate = new Date(startDate);
        Date sTime = new Date(startTime);

        if (sDate != null){
            Log.e("Date", sDate.toString());
        }
        if (sTime != null){
            Log.e("Time", sTime.toString());
        }
//        event.setEventStartDate(sDate);
//        event.setEventStartTime(sTime);

        Log.e("Retry", "Recreated Event");

        startNotification();
    }

    private EventTypeEnum getTye(int type) {
        switch(type){
            case 0:
                return EventTypeEnum.CASUAL;
            case 1:
                return EventTypeEnum.FORMAL;
            case 2:
                return EventTypeEnum.MUSIC;
            case 3:
                return EventTypeEnum.SPORT;
            default:
                return null;
        }
    }

    private PendingIntent createRetryIntent() {
        Intent retryIntent = new Intent(this, CreateEventActivity.class);
        Bundle bundle = new Bundle();
//        bundle.putString("Name", event.getEventName());
//        bundle.putString("Description", event.getEventDescription());
//        bundle.putString("Photo Path", event.getCoverPhotoPath());
//        bundle.putDouble("Longitude", event.getEventLocation().getLongitude());
//        bundle.putDouble("Latitude", event.getEventLocation().getLatitude());
//        bundle.putInt("Type", event.getEventType());
//        bundle.putLong("Start Date", event.getEventStartDate().getTime());
//        bundle.putLong("Start Time", event.getEventStartTime().getTime());

        retryIntent.putExtras(bundle);

        PendingIntent retryPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        retryIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        return retryPendingIntent;
    }

//    @Override
//    public void onEventSaveFailInteraction() {
//        Log.e("Fail", "Called 1");
//        retryNotification();
//    }

    private void retryNotification() {
        PendingIntent retryIntent = createRetryIntent();
        mBuilder.setContentTitle("Something went wrong")
                .setProgress(0, 0, false)
                .addAction(R.drawable.ic_action_create, "Retry", retryIntent)
                .setContentIntent(retryIntent)
                .setContentText("Click to try again");

        mNotificationManager.notify(
                1,
                mBuilder.build());
    }

//    @Override
//    public void onFileSaveFailInteraction() {
//        Log.e("Fail", "Called 2");
//        retryNotification();
//    }

    public enum FragmentShownState {
        EVENT_LOCATION,
        EVENT_COVER_PHOTO,
        PHOTO_PREVIEW,
        EVENT_NAME,
        EVENT_INFO,
        CREATING_EVENT
    }
}

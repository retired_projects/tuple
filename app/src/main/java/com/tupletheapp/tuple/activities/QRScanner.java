package com.tupletheapp.tuple.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.DynamoDBMapper;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.custom_java.Attending;
import com.tupletheapp.tuple.custom_java.Event;
import com.tupletheapp.tuple.utils.AmazonUtil;

public class QRScanner extends ActionBarActivity {

    private DynamoDBMapper mapper;
    private final int SKIP_ON_BACK = 8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrscanner);

        IntentIntegrator scanIntergrator = new IntentIntegrator(this);
        scanIntergrator.addExtra("PROMPT_MESSAGE", "Scan valid Tuple Event code to follow");
        scanIntergrator.initiateScan();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == SKIP_ON_BACK)
            finish();

        else {
            IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);

            String eventId = scanningResult.getContents();

            if (eventId != null) {
                Log.e("EventID", eventId);
                mapper = AmazonUtil.getMapper(this);
                Event followedEvent = mapper.load(Event.class, eventId);

                if (followedEvent != null) {
                    //TODO check if user is already attending

                    Toast toast = Toast.makeText(getApplicationContext(),
                            "Scan Data found!", Toast.LENGTH_SHORT);
                    toast.show();
                    Attending attending = new Attending();
                    attending.setEventId(eventId);
                    //TODO get current user id
                    attending.setUserId("1");
                    attending.setEventDate(followedEvent.testDate());

                    mapper.save(attending);
                    goToFollowedEvent(eventId);

                } else {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "Scan Data not valid", Toast.LENGTH_SHORT);
                    toast.show();

                    finish();
                }

            } else {
                finish();
            }
        }
    }

    private void goToFollowedEvent(String eventId) {

        Log.e("EventID", eventId);
        Bundle bundle = new Bundle();

        bundle.putString("eventID", eventId);
        Intent intent = new Intent(this, EventActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, SKIP_ON_BACK);
    }
}

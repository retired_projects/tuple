package com.tupletheapp.tuple.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.tupletheapp.tuple.R;

public class FullScreenActivity extends ActionBarActivity {
    private ImageView fullScreenImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_full_screen);
        fullScreenImage = (ImageView) findViewById(R.id.full_screen_image);

        Glide.with(this)
                .load(getIntent().getExtras().getString("photoUrl"))
                .into(fullScreenImage);
    }

}

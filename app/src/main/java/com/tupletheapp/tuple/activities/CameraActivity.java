package com.tupletheapp.tuple.activities;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.hardware.Camera;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.tupletheapp.tuple.Dialog.ImageProgressDialog;
import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.async_tasks.UploadContentTask;
import com.tupletheapp.tuple.custom_java.EventContent;
import com.tupletheapp.tuple.custom_views.CameraPreview;
import com.tupletheapp.tuple.custom_views.VideoPreview;
import com.tupletheapp.tuple.fragments.SelectEventCoverPhotoFragment;
import com.tupletheapp.tuple.fragments.SelectEventPhotoPreviewFragment;

public class CameraActivity extends ActionBarActivity implements
        SelectEventCoverPhotoFragment.SelectCoverPhotoOnFragmentInteractionListener,
        SelectEventPhotoPreviewFragment.SelectEventPhotoPreviewOnFragmentInteractionListener,
        UploadContentTask.UploadContentListener{
    private SelectEventCoverPhotoFragment cameraFragment;
    private SelectEventPhotoPreviewFragment eventPhotoPreviewFragment;

    private ImageProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_camera);

        cameraFragment = new SelectEventCoverPhotoFragment();
        eventPhotoPreviewFragment = SelectEventPhotoPreviewFragment.newInstance();

        dialog = ImageProgressDialog.newInstance();

        getFragmentManager()
                .beginTransaction()
                .add(R.id.container, cameraFragment)
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void SelectCoverPhotoOnFragmentInteraction(String coverPhoto) {
        eventPhotoPreviewFragment.setCoverPhotoPath(coverPhoto);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.container, eventPhotoPreviewFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void SelectEventPhotoPreviewOnFragmentInteraction(String coverPhotoPath) {
        EventContent content = new EventContent(
                null,
                "1",
                getIntent().getExtras().getString("eventID"),
                true,
                coverPhotoPath
        );

        new UploadContentTask(this, this).execute(content);
    }

    public void showDialog(){
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(R.id.container, dialog);
        transaction.show(dialog);
        transaction.commit();
    }


    public void dismissDialog(){
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.remove(dialog);
        transaction.commit();
    }

    @Override
    public void finishSave(EventContent content) {
        dismissDialog();
        Toast.makeText(this, "Event Content has been saved", Toast.LENGTH_LONG).show();

        Intent intent = new Intent();

        Bundle bundle = new Bundle();
        bundle.putString("contentID", content.getID());
        bundle.putString("contentCreatorID", content.getCreatorID());
        bundle.putString("contentEventID", content.getEventID());
        bundle.putBoolean("contentType", content.getType());
        bundle.putString("contentUrl", content.getUrl());

        intent.putExtras(bundle);

        setResult(RESULT_OK, intent);
        finish();
    }
}

package com.tupletheapp.tuple.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.animators.FadeAnimator;
import com.tupletheapp.tuple.custom_views.TouchImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ContentViewActivity extends ActionBarActivity implements View.OnClickListener {
    private TouchImageView fullscreenPhoto;
    private TextView fullScreenComment;
    private View contentViewRoot;
    private TextView userName;
    private ImageButton saveContentButton;

    private int contentType;
    private boolean showUserInfo;

    private String eventContentObjectId;

    private int[] colors;

    private FadeAnimator fadeAnimator;

    private Matrix matrix = new Matrix();
    private float scale = 1f;
    private ScaleGestureDetector SGD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_view);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        contentType = Integer.parseInt(getIntent().getExtras().getString("contentType"));
        eventContentObjectId = getIntent().getExtras().getString("eventContentObjectId");
        fadeAnimator = new FadeAnimator();
        colors = getResources().getIntArray(R.array.color_swatch);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        fullScreenComment = (TextView) findViewById(R.id.fullscreen_comment);
        fullscreenPhoto = (TouchImageView) findViewById(R.id.fullscreen_photo);
        userName = (TextView) findViewById(R.id.user_name);
        contentViewRoot = findViewById(R.id.content_view_root);
        saveContentButton = (ImageButton) findViewById(R.id.save_button);

        showUserInfo = true;

        if (contentType == getResources().getInteger(R.integer.comment_request_code)) {
            contentViewRoot.setBackgroundColor(getResources().getColor(R.color.tuple_black));
            fullScreenComment.setVisibility(View.VISIBLE);
            fullScreenComment.setText(getIntent().getExtras().getString("comment"));
            fullScreenComment.setOnClickListener(this);

        } else if (contentType == getResources().getInteger(R.integer.photo_request_code)) {
            fullscreenPhoto.setVisibility(View.VISIBLE);
            String photoUrl = getIntent().getExtras().getString("photoUrl");

            Glide.with(this)
                    .load(photoUrl)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            contentNotFound();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            saveContentButton.setVisibility(ImageButton.VISIBLE);
                            return false;
                        }
                    })
                    .placeholder(R.drawable.tuple_loading_image)
                    .crossFade()
                    .into(fullscreenPhoto);

            fullscreenPhoto.setMaxZoom(2f);
            fullscreenPhoto.setActivity(this);
        }

        saveContentButton.setOnClickListener(this);
    }

    private void contentNotFound(){
        Toast.makeText(this, "Sorry the content was deleted", Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save_button:
                Toast.makeText(this, "Saving Image", Toast.LENGTH_SHORT).show();
                saveContent();
                break;
        }
    }

    private void saveContent(){
        fullscreenPhoto.buildDrawingCache();
        Bitmap bmp = fullscreenPhoto.getDrawingCache();

        File storageLoc = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES+"/tuple/"); //context.getExternalFilesDir(null);

        File file = new File(storageLoc, eventContentObjectId + ".jpg");

        try{
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();

            scanFile(Uri.fromFile(file));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void scanFile( Uri imageUri){
        Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        scanIntent.setData(imageUri);
        sendBroadcast(scanIntent);
    }

}

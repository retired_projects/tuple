package com.tupletheapp.tuple.activities;



import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;


import android.support.v7.widget.Toolbar;
import android.view.MenuItem;


import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.fragments.ForgotPasswordEmailSentConfirmationFragment;
import com.tupletheapp.tuple.fragments.ForgotPasswordRetrievePasswordMethodFragment;
import com.tupletheapp.tuple.fragments.ForgotPasswordEmailFragment;
import com.tupletheapp.tuple.fragments.ForgotPasswordUsernameFragment;

public class ForgotPasswordActivity extends AppCompatActivity
        implements ForgotPasswordEmailFragment.OnFragmentInteractionListener,
        ForgotPasswordEmailSentConfirmationFragment.EmailSentConfirmationOnFragmentInteractionListener,
        ForgotPasswordRetrievePasswordMethodFragment.RetrievePasswordMethodOnFragmentInteractionListener,
        ForgotPasswordUsernameFragment.UserUsernameOnFragmentInteractionListener{

    private final String USER_EMAIL_FRAGMENT_TAG = "userEmailFragment";
    private final String EMAIL_SENT_CONFIRMATION_FRAGMENT_TAG = "emailSentConfirmationFragment";
    private final String RETRIEVE_PASSWORD_METHOD_FRAGMENT_TAG = "retrievePasswordMethodFragment";
    private final String USER_USERNAME_FRAGMENT_TAG = "userUsernameFragment";

    private Toolbar toolbar;

    private ForgotPasswordEmailFragment userEmailFragment;
    private ForgotPasswordEmailSentConfirmationFragment emailSentConfirmationFragment;
    private ForgotPasswordRetrievePasswordMethodFragment retrievePasswordMethodFragment;
    private ForgotPasswordUsernameFragment userUsernameFragment;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        userEmailFragment = new ForgotPasswordEmailFragment();
        retrievePasswordMethodFragment = new ForgotPasswordRetrievePasswordMethodFragment();
        userUsernameFragment = new ForgotPasswordUsernameFragment();

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        initToolbar();

        showRetrievePasswordMethodFragment();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if(fm.getBackStackEntryCount() > 1)
            super.onBackPressed();
        else
            finish();
    }

    public Toolbar getToolbar(){
        return toolbar;
    }

    public void initToolbar(){
        toolbar.setTitle("Forgot Password");
        setSupportActionBar(toolbar);
    }

    private void showRetrievePasswordMethodFragment(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.forgot_password_container, retrievePasswordMethodFragment, RETRIEVE_PASSWORD_METHOD_FRAGMENT_TAG);
        transaction.addToBackStack(RETRIEVE_PASSWORD_METHOD_FRAGMENT_TAG);
        transaction.commit();
    }

    private void showUserEmailFragment(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.forgot_password_container, userEmailFragment, USER_EMAIL_FRAGMENT_TAG);
        transaction.addToBackStack(USER_EMAIL_FRAGMENT_TAG);
        transaction.commit();
    }

    private void showEmailSentConfirmationFragment(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.forgot_password_container, emailSentConfirmationFragment, EMAIL_SENT_CONFIRMATION_FRAGMENT_TAG);
        transaction.addToBackStack(EMAIL_SENT_CONFIRMATION_FRAGMENT_TAG);
        transaction.commit();
    }

    private void showUsernameFragment(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.forgot_password_container, userUsernameFragment, USER_USERNAME_FRAGMENT_TAG);
        transaction.addToBackStack(USER_USERNAME_FRAGMENT_TAG);
        transaction.commit();
    }

    @Override
    public void onFragmentInteraction(String username, String email, String userAvatarUrl) {
        emailSentConfirmationFragment = ForgotPasswordEmailSentConfirmationFragment.newInstance(username, email, userAvatarUrl);
        showEmailSentConfirmationFragment();
    }

    @Override
    public void EmailSentConfirmationOnFragmentInteraction() {
        Intent intent = new Intent(this, TupleLoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void RetrievePasswordUsingEmail() {
        showUserEmailFragment();
    }

    public void RetrievePasswordUsingUsername(){
        showUsernameFragment();
    }

    @Override
    public void UserUsernameOnFragmentInteractionListener(String username, String email, String userAvatarUrl) {
        emailSentConfirmationFragment = ForgotPasswordEmailSentConfirmationFragment.newInstance(username, email, userAvatarUrl);
        showEmailSentConfirmationFragment();
    }
}

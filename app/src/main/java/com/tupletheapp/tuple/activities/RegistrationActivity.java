package com.tupletheapp.tuple.activities;

import android.os.Bundle;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;

import com.tupletheapp.tuple.R;
import com.tupletheapp.tuple.custom_java.NewUserInformation;
import com.tupletheapp.tuple.fragments.RegistrationCreatingProfileFragment;
import com.tupletheapp.tuple.fragments.RegistrationEmailFragment;
import com.tupletheapp.tuple.fragments.RegistrationGettingStartedFragment;
import com.tupletheapp.tuple.fragments.RegistrationPasswordFragment;
import com.tupletheapp.tuple.fragments.RegistrationUsernameFragment;

public class RegistrationActivity extends  AppCompatActivity
        implements RegistrationGettingStartedFragment.OnFragmentInteractionListener,
        RegistrationEmailFragment.EmailOnFragmentInteractionListener,
        RegistrationUsernameFragment.UsernameOnFragmentInteractionListener,
        RegistrationPasswordFragment.PasswordOnFragmentInteractionListener ,
        RegistrationCreatingProfileFragment.CreatingProfileOnFragmentInteractionListener{

    private final String GETTING_STARTED_FRAGMENT_TAG = "gettingStartedFragment";
    private final String EMAIL_FRAGMENT_TAG = "emailFragment";
    private final String USERNAME_FRAGMENT_TAG = "usernameFragment";
    private final String PASSWORD_FRAGMENT_TAG = "passwordFragment";
    private final String CREATING_PROFILE_TAG = "creatingProfileFragment";

    private NewUserInformation newUser;

    private float x1,x2;
    static final int MIN_DISTANCE = 150;

    private RegistrationGettingStartedFragment gettingStartedFragment;
    private RegistrationEmailFragment emailFragment;
    private RegistrationUsernameFragment usernameFragment;
    private RegistrationPasswordFragment passwordFragment;
    private RegistrationCreatingProfileFragment creatingProfileFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        newUser = new NewUserInformation();

        gettingStartedFragment = new RegistrationGettingStartedFragment();
        emailFragment = new RegistrationEmailFragment();
        usernameFragment = new RegistrationUsernameFragment();
        passwordFragment = new RegistrationPasswordFragment();
        creatingProfileFragment = new RegistrationCreatingProfileFragment();

        showGettingStartedFragment();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                float deltaX = x2 - x1;
                if (Math.abs(deltaX) > MIN_DISTANCE && x2 > x1)
                    onBackPressed();
                break;
        }
        return super.onTouchEvent(event);
    }

    public void onBackPressed(){
        FragmentManager fm = getSupportFragmentManager();
        if(fm.getBackStackEntryCount() > 1)
            super.onBackPressed();
        else
            finish();
    }


    private void showGettingStartedFragment(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.registration_container, gettingStartedFragment, GETTING_STARTED_FRAGMENT_TAG);
        transaction.addToBackStack(GETTING_STARTED_FRAGMENT_TAG);
        transaction.commit();
    }

    private void showEmailFragment(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.registration_container, emailFragment, EMAIL_FRAGMENT_TAG);
        transaction.addToBackStack(EMAIL_FRAGMENT_TAG);
        transaction.commit();
    }


    private void showUsernameFragment(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.registration_container, usernameFragment, USERNAME_FRAGMENT_TAG);
        transaction.addToBackStack(USERNAME_FRAGMENT_TAG);
        transaction.commit();
    }

    private void showPasswordFragment(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.registration_container, passwordFragment, PASSWORD_FRAGMENT_TAG);
        transaction.addToBackStack(PASSWORD_FRAGMENT_TAG);
        transaction.commit();
    }

    private void showCreatingProfile(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.registration_container, creatingProfileFragment, CREATING_PROFILE_TAG);
        transaction.addToBackStack(CREATING_PROFILE_TAG);
        transaction.commit();
    }

    @Override
    public void onFragmentInteraction() {
        showEmailFragment();
    }

    @Override
    public void emailOnFragmentInteraction(String email) {
        newUser.setEmail(email);
        showUsernameFragment();
    }

    @Override
    public void UsernameOnFragmentInteraction(String username){
        newUser.setUsername(username);
        showPasswordFragment();
    }

    @Override
    public void PasswordOnFragmentInteraction(String password) {
        newUser.setPassword(password);
        showCreatingProfile();
    }

    @Override
    public void CreatingProfileOnFragmentInteraction() {

    }
}

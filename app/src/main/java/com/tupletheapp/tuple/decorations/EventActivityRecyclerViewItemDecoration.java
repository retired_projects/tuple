package com.tupletheapp.tuple.decorations;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class EventActivityRecyclerViewItemDecoration extends RecyclerView.ItemDecoration{
    private int padding;

    public EventActivityRecyclerViewItemDecoration(int padding) {
        this.padding = padding;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,RecyclerView parent, RecyclerView.State state) {
        outRect.bottom = padding;
    }
}

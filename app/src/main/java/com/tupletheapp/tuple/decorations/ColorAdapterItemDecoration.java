package com.tupletheapp.tuple.decorations;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by ghigareda on 1/3/2016.
 */
public class ColorAdapterItemDecoration extends RecyclerView.ItemDecoration{
    private int padding;

    public ColorAdapterItemDecoration(int padding) {
        this.padding = padding;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.top = padding;
        outRect.bottom = padding;
        outRect.left = padding;
        outRect.right = padding;
    }
}

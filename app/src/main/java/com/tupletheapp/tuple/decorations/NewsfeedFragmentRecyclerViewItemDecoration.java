package com.tupletheapp.tuple.decorations;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by ghigareda on 7/24/2015.
 */
public class NewsfeedFragmentRecyclerViewItemDecoration extends RecyclerView.ItemDecoration{
    private int padding;

    public NewsfeedFragmentRecyclerViewItemDecoration(int padding) {
        this.padding = padding;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.bottom = padding;
    }
}

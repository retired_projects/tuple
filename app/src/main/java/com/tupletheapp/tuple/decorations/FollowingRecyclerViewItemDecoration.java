package com.tupletheapp.tuple.decorations;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by ghigareda on 6/22/2015.
 */
public class FollowingRecyclerViewItemDecoration extends RecyclerView.ItemDecoration{
    private int space;

    public FollowingRecyclerViewItemDecoration(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        if(!isTopRow(view, parent))
            outRect.top = space;
    }

    private boolean isTopRow(View view, RecyclerView parent){
        return parent.getChildPosition(view) == 0;
    }
}
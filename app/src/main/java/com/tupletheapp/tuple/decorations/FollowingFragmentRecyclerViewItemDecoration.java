package com.tupletheapp.tuple.decorations;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by ghigareda on 6/28/2015.
 */
public class FollowingFragmentRecyclerViewItemDecoration extends RecyclerView.ItemDecoration{
    private int padding;

    public FollowingFragmentRecyclerViewItemDecoration(int padding) {
        this.padding = padding;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,RecyclerView parent, RecyclerView.State state) {
        if(isTopRow(view, parent))
            outRect.top = padding;


        if(isLeftRow(view, parent)){
            outRect.left = padding;
            outRect.right = padding/2;
        }else if(isRightRow(view, parent)){
            outRect.left = padding/2;
            outRect.right = padding;
        }else{
            outRect.left = padding/2;
            outRect.right = padding/2;
        }

        outRect.bottom = padding;

    }

    private boolean isTopRow(View view, RecyclerView parent){
        return (parent.getChildPosition(view) == 0 || parent.getChildPosition(view) == 1 || parent.getChildPosition(view) == 2);
    }

    private boolean isLeftRow(View view, RecyclerView parent){
        return parent.getChildPosition(view) % 3 == 0;
    }

    private boolean isRightRow(View view, RecyclerView parent){
        return parent.getChildPosition(view) % 3 == 2;
    }
}

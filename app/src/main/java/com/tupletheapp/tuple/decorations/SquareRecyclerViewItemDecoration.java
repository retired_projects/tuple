package com.tupletheapp.tuple.decorations;


import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class SquareRecyclerViewItemDecoration extends RecyclerView.ItemDecoration{
    private int space;

    public SquareRecyclerViewItemDecoration(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        if(isLeftRow(view, parent)){
            outRect.bottom = space;
            outRect.left = space;
            outRect.top = 0;
            outRect.right = 0;
        }else if(isMiddleRow(view, parent)){
            outRect.bottom = space;
            outRect.left = space;
            outRect.top = 0;
            outRect.right = space;
        }
        else if(isRightRow(view, parent)){
            outRect.bottom = space;
            outRect.left = 0;
            outRect.top = 0;
            outRect.right = space;
        }
//
//        if(isTopRow(view, parent))
//            outRect.top = space;
    }

    private boolean isTopRow(View view, RecyclerView parent){
        return (parent.getChildPosition(view) == 0
                || parent.getChildPosition(view) == 1
                || parent.getChildPosition(view) == 2);
    }

    private boolean isLeftRow(View view, RecyclerView parent){
        return parent.getChildPosition(view) % 3 == 0;
    }

    private boolean isMiddleRow(View view, RecyclerView parent){
        return parent.getChildPosition(view) % 3 == 1;
    }



    private boolean isRightRow(View view, RecyclerView parent){
        return parent.getChildPosition(view) % 3 == 2;
    }
}

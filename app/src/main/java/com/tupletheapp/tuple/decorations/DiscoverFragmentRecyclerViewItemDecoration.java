package com.tupletheapp.tuple.decorations;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by ghigareda on 6/29/2015.
 */
public class DiscoverFragmentRecyclerViewItemDecoration extends RecyclerView.ItemDecoration {
    private int padding;

    public DiscoverFragmentRecyclerViewItemDecoration(int padding) {
        this.padding = padding;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if(isTopRow(view, parent))
            outRect.top = padding;
        outRect.bottom = padding;
        outRect.left = padding;
        outRect.right = padding;
    }

    private boolean isTopRow(View view, RecyclerView parent) {
        return (parent.getChildPosition(view) == 0);
    }
}

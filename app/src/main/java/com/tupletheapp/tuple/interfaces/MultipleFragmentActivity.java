package com.tupletheapp.tuple.interfaces;

import android.app.Fragment;

/**
 * Created by ghigareda on 11/18/2015.
 */
public interface MultipleFragmentActivity {
    public void fragmentSetup();
}

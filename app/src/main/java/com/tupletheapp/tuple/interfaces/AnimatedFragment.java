package com.tupletheapp.tuple.interfaces;

/**
 * Created by ghigareda on 11/30/2015.
 */
public interface AnimatedFragment {
    public void fragmentShown();
}

package com.tupletheapp.tuple.interfaces;

import android.view.ViewGroup;

import com.tupletheapp.tuple.callbacks.ObservableScrollViewCallbacks;

public interface Scrollable {
    void setScrollViewCallbacks(ObservableScrollViewCallbacks listener);
    void scrollVerticallyTo(int y);
    int getCurrentScrollY();
    void setTouchInterceptionViewGroup(ViewGroup viewGroup);
}
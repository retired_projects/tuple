package com.tupletheapp.tuple.utils;

import android.app.Activity;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ghigareda on 5/18/2015.
 */
public class DateFormatUtil {
    private final String PARSE_DATE_FORMAT = "MMM dd, yyyy";
    private final String PARSE_DATE_FORMAT_2 = "MM/dd/yyyy hh:mm a";
    private static DateFormatUtil instance = null;

    protected DateFormatUtil(){

    }

    public static DateFormatUtil getInstance(){
        if(instance == null)
            instance = new DateFormatUtil();
        return instance;
    }

    public Date stringToDateConversion(String dateString){
        try {
            return  new SimpleDateFormat(PARSE_DATE_FORMAT_2).parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String datetoStringConversion(Date dateTime){
        return  new SimpleDateFormat(PARSE_DATE_FORMAT).format(dateTime);
    }
}

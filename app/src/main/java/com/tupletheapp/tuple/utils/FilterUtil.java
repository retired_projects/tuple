package com.tupletheapp.tuple.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuffXfermode;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.renderscript.ScriptIntrinsicConvolve3x3;

import com.tupletheapp.tuple.R;

public class FilterUtil {
    private static FilterUtil instance = null;

    public FilterUtil(){

    }

    public static FilterUtil getInstance(){
        if(instance == null)
            instance = new FilterUtil();
        return instance;
    }

    public Bitmap applyWhiteWallsFilter(Context context, Bitmap originalBitmap){
        Bitmap bitmap = getBitmapCopy(originalBitmap);

        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0);

        ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setColorFilter(colorFilter);
        canvas.drawBitmap(originalBitmap, 0, 0, paint);

        return bitmap;
    }

    public Bitmap applyViridianFilter(Context context, Bitmap originalBitmap){
        Bitmap canvasBitmap = Bitmap.createBitmap(originalBitmap.getWidth(), originalBitmap.getHeight(), originalBitmap.getConfig());
        Bitmap light = getBitmapCopy(originalBitmap);
        Bitmap blue = getBitmapCopy(originalBitmap);

        Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.light_leak);

        PorterDuffColorFilter porterDuffColorFilter =  new PorterDuffColorFilter(Color.argb(110, 255, 255, 255), PorterDuff.Mode.LIGHTEN);
        PorterDuffColorFilter porterDuffColorFilterRed =  new PorterDuffColorFilter(Color.argb(86, 255, 0,0), PorterDuff.Mode.SCREEN);
        PorterDuffColorFilter porterDuffColorFilterBlue =  new PorterDuffColorFilter(Color.argb(40, 0,76,170), PorterDuff.Mode.SCREEN);

        Paint MyPaint_PorterDuff = new Paint();
        MyPaint_PorterDuff.setColorFilter(porterDuffColorFilterRed);

        Paint secondPaint = new Paint();
        secondPaint.setColorFilter(porterDuffColorFilter);

        Paint thirdPaint = new Paint();
        secondPaint.setColorFilter(porterDuffColorFilterBlue);

        Paint fourthPaint = new Paint();
        fourthPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SCREEN));

        Canvas canvas = new Canvas(canvasBitmap);
        canvas.drawBitmap(originalBitmap, 0, 0, MyPaint_PorterDuff);
        canvas.drawBitmap(blue, 0, 0, thirdPaint);
        canvas.drawBitmap(light, 0,0, secondPaint);
        canvas.drawBitmap(bm, -500, -500, fourthPaint);

        return canvasBitmap;
    }

    public Bitmap applyParallaxFilter(Context context, Bitmap originalBitmap){
        float contrast = 1.05f;
        float brightnessFloat = 20;
        ColorMatrix cm = new ColorMatrix(new float[]
                {
                        contrast, 0, 0, 0, brightnessFloat,
                        0, contrast, 0, 0, brightnessFloat,
                        0, 0, contrast, 0, brightnessFloat,
                        0, 0, 0, 1, 0
                });

        adjustHue(cm, 20);
        adjustSaturataion(cm, .79f);

        Bitmap ret = Bitmap.createBitmap(originalBitmap.getWidth(), originalBitmap.getHeight(), originalBitmap.getConfig());

        Canvas canvas = new Canvas(ret);

        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(cm));

        Paint secondLayerPaint = new Paint();
        secondLayerPaint.setColorFilter(new ColorMatrixColorFilter(cm));
        secondLayerPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.OVERLAY));

        float brightnessTemp = -50;
        ColorMatrix cm2 = new ColorMatrix(new float[]{
                1, 0, 0, 0, brightnessTemp,
                0, 1, 0, 0, brightnessTemp,
                0, 0, 1, 0, brightnessTemp,
                0, 0, 0, 1, 0
        });

        adjustSaturataion(cm2, 1.27f);

        Paint thirdLayerPaint = new Paint();
        thirdLayerPaint.setColorFilter(new ColorMatrixColorFilter(cm2));
        thirdLayerPaint.setAlpha(40);

        canvas.drawBitmap(originalBitmap, 0, 0, paint);
        canvas.drawBitmap(originalBitmap, 0, 0, secondLayerPaint);
        canvas.drawBitmap(originalBitmap, 0, 0, thirdLayerPaint);

        return ret;
    }

    public void adjustHue(ColorMatrix cm, float value){
        value = cleanValue(value, 180f) / 180f * (float) Math.PI;
        if (value == 0)
        {
            return;
        }
        float cosVal = (float) Math.cos(value);
        float sinVal = (float) Math.sin(value);
        float lumR = 0.213f;
        float lumG = 0.715f;
        float lumB = 0.072f;
        float[] mat = new float[]
                {
                        lumR + cosVal * (1 - lumR) + sinVal * (-lumR), lumG + cosVal * (-lumG) + sinVal * (-lumG), lumB + cosVal * (-lumB) + sinVal * (1 - lumB), 0, 0,
                        lumR + cosVal * (-lumR) + sinVal * (0.143f), lumG + cosVal * (1 - lumG) + sinVal * (0.140f), lumB + cosVal * (-lumB) + sinVal * (-0.283f), 0, 0,
                        lumR + cosVal * (-lumR) + sinVal * (-(1 - lumR)), lumG + cosVal * (-lumG) + sinVal * (lumG), lumB + cosVal * (1 - lumB) + sinVal * (lumB), 0, 0,
                        0f, 0f, 0f, 1f, 0f,
                        0f, 0f, 0f, 0f, 1f };
        cm.postConcat(new ColorMatrix(mat));
    }

    public void adjustSaturataion(ColorMatrix cm, float value){
        ColorMatrix saturationMatrix = new ColorMatrix();
        saturationMatrix.setSaturation(value);
        cm.postConcat(saturationMatrix);
    }

    private float cleanValue(float p_val, float p_limit){
        return Math.min(p_limit, Math.max(-p_limit, p_val));
    }



    private Bitmap getBitmapCopy(Bitmap originalBitmap){
        int height = originalBitmap.getHeight();
        int width = originalBitmap.getWidth();
        return Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
    }
}

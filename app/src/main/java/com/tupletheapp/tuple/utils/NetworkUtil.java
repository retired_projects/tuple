package com.tupletheapp.tuple.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Hector on 6/5/2015.
 */
public class NetworkUtil {
    private static NetworkUtil instance = null;

    protected NetworkUtil(){

    }

    public static NetworkUtil getInstance(){
        if(instance == null)
            instance = new NetworkUtil();
        return instance;
    }

    public boolean isOnline(Activity activity) {
        ConnectivityManager connMgr = (ConnectivityManager)
                activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }
}

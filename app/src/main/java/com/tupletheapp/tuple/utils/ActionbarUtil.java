package com.tupletheapp.tuple.utils;

import android.content.Context;
import android.content.res.TypedArray;

import com.tupletheapp.tuple.R;

/**
 * Created by ghigareda on 5/25/2015.
 */
public class ActionbarUtil {
    private static ActionbarUtil instance = null;

    protected ActionbarUtil(){

    }

    public static ActionbarUtil getInstance(){
        if(instance == null)
            instance = new ActionbarUtil();
        return instance;
    }

    public int getToolbarHeight(Context context) {
        final TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
                new int[]{R.attr.actionBarSize});
        int toolbarHeight = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        return toolbarHeight;
    }
}

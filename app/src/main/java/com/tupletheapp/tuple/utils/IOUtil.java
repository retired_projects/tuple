package com.tupletheapp.tuple.utils;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ghigareda on 11/18/2015.
 */
public class IOUtil {
    private static IOUtil instance = null;

    private final String TIME_STAMP_FORMAT = "yyyyMMdd_HHmmss";
    private final String IMAGE_EXTENSION = ".jpg";
    private final String DIRECTORY_EXTENSION = "/Tuple";

    protected IOUtil() {

    }

    public static IOUtil getInstance() {
        if(instance == null) {
            instance = new IOUtil();
        }
        return instance;
    }

    public File createImageFile() {

        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + DIRECTORY_EXTENSION);

        if(!storageDir.exists())
            storageDir.mkdirs();

        File image = null;
        try {
            image = File.createTempFile(
                    generateImageFileName(),
                    IMAGE_EXTENSION,
                    storageDir
            );
        } catch (IOException e) {
            Log.e("Tuple", e.toString());
        }
        return image;
    }

    private String generateImageFileName(){
        String timeStamp = new SimpleDateFormat(TIME_STAMP_FORMAT).format(new Date());
        return "JPEG_" + timeStamp + "_";
    }
}

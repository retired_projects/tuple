package com.tupletheapp.tuple.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MediaUtil {
    private static MediaUtil instance = null;
    private final int DEFAULT_HEIGHT_LIMIT = 2000;
    private final int DEFAULT_WIDTH_LIMIT = 2000;
    private Fragment fragment;
    private Activity activity;

    protected MediaUtil() {

    }

    public static MediaUtil getInstance() {
        if (instance == null)
            instance = new MediaUtil();
        return instance;
    }

    public Bitmap generateThumbnail(Bitmap originalBitmap) {
        return ThumbnailUtils.extractThumbnail(originalBitmap, 500, 350);
    }

    public Bitmap generateContentThumbnail(Bitmap originalBitmap) {
        return ThumbnailUtils.extractThumbnail(originalBitmap, 350, 350);
    }

    public Bitmap resizeBitmap(Bitmap originalBitmap) {
        return resizeBitmap(originalBitmap, DEFAULT_HEIGHT_LIMIT, DEFAULT_WIDTH_LIMIT);
    }

    public Bitmap resizeBitmap(Bitmap originalBitmap, int heightLimit, int widthLimit) {
        int bitmapHeight = originalBitmap.getHeight();
        int bitmapWidth = originalBitmap.getWidth();
        int newWidth = 1;
        int newHeight = 1;
        float multFactor;
        Bitmap resizedBitmap;

        if (bitmapHeight <= heightLimit && bitmapWidth <= widthLimit) {
            return originalBitmap;
        }
        if (bitmapHeight > bitmapWidth) {
            newHeight = heightLimit;
            multFactor = (float) bitmapWidth / (float) bitmapHeight;
            newWidth = (int) (newHeight * multFactor);
        } else if (bitmapWidth > bitmapHeight) {
            newWidth = widthLimit;
            multFactor = (float) bitmapHeight / (float) bitmapWidth;
            newHeight = (int) (newWidth * multFactor);
        } else if (bitmapHeight == bitmapWidth) {
            newHeight = heightLimit;
            newWidth = widthLimit;
        }
        resizedBitmap = Bitmap.createScaledBitmap(originalBitmap, newWidth, newHeight, false);
        return resizedBitmap;
    }


    public Bitmap getBitmap(int resultCode, Uri selectedImageUri, Activity activity, String coverPhotoAbsolutePath) {
        if (resultCode == Activity.RESULT_OK) {
            Bitmap coverPhoto;
            Bitmap resizedBitmap;


            if (Build.VERSION.SDK_INT < 19) {
                coverPhoto = BitmapFactory.decodeFile(coverPhotoAbsolutePath);
                resizedBitmap = resizeBitmap(coverPhoto);
                return orientateBitmap(resizedBitmap, coverPhotoAbsolutePath);
            } else {
                ParcelFileDescriptor parcelFileDescriptor = generateFileDescriptor(selectedImageUri, activity);
                FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
                coverPhoto = BitmapFactory.decodeFileDescriptor(fileDescriptor);
                closeFileDescriptor(parcelFileDescriptor);
                resizedBitmap = resizeBitmap(coverPhoto);
                return orientateBitmap(resizedBitmap, coverPhotoAbsolutePath);
            }
        }
        return null;
    }

    public Bitmap orientateBitmap(Bitmap bitmap, Intent data, Activity activity){
        return orientateBitmap(bitmap, getPath(data.getData(), activity));
    }

    public Bitmap orientateBitmap(Bitmap bitmap, String path) {
        File imageFile = new File(path);
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(imageFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        boolean isVertical = false;
        boolean is90Degree = false;
        boolean is180Degree = false;
        boolean is270Degree = false;

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_270:
                is270Degree = true;
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                is90Degree = true;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                is180Degree = true;
                break;
            default:
                isVertical = true;
                break;
        }
        if (isVertical)
            return bitmap;
        else if (is90Degree) {
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        } else if (is180Degree) {
            Matrix matrix = new Matrix();
            matrix.postRotate(180);
            return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        } else if (is270Degree) {
            Matrix matrix = new Matrix();
            matrix.postRotate(270);
            return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        }
        return null;
    }

    private ParcelFileDescriptor generateFileDescriptor(Uri selectedImageUri, Activity activity) {
        try {
            return activity.getContentResolver().openFileDescriptor(selectedImageUri, "r");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void closeFileDescriptor(ParcelFileDescriptor parcelFileDescriptor) {
        try {
            parcelFileDescriptor.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("NewApi")
    public String getPath(Uri uri, Activity activity) {
        final boolean needToCheckUri = Build.VERSION.SDK_INT >= 19;
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        // deal with different Uris.
        if (needToCheckUri && DocumentsContract.isDocumentUri(activity, uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[] {
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    protected static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    protected static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    protected static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


    public File bitmapToFile(Bitmap bitmap, String fileName){

        File storageLoc = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES); //context.getExternalFilesDir(null);

        File file = new File(storageLoc, fileName + "tmp.jpg");

        try{
            FileOutputStream fos = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, fos);
            fos.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return file;
    }

}
package com.tupletheapp.tuple.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.tupletheapp.tuple.R;


public class LocationServiceUtil {
    private static LocationServiceUtil instance = null;
    static boolean isGPSEnabled = false;

    protected LocationServiceUtil() {

    }

    public static LocationServiceUtil getInstance() {
        if (instance == null)
            instance = new LocationServiceUtil();
        return instance;
    }

    public void displaySettingsAlert(Activity activity){
        GPSTracker gpsTracker = new GPSTracker(activity);
        if(!gpsTracker.getIsGPSTrackingEnabled()){
            gpsTracker.showSettingsAlert();

        }
    }

    public LatLng getUserLocation(Activity activity){
        GPSTracker gpsTracker = new GPSTracker(activity);
        return new LatLng(gpsTracker.latitude, gpsTracker.longitude);
    }

    public boolean isGPSEnabled(){
        return isGPSEnabled;
    }

    public class GPSTracker extends Service implements LocationListener {
        private final Activity mContext;

        // flag for GPS Status
        boolean isGPSEnabled = false;

        // flag for network status
        boolean isNetworkEnabled = false;

        // flag for GPS Tracking is enabled
        boolean isGPSTrackingEnabled = false;

        Location location;
        double latitude;
        double longitude;

        // How many Geocoder should return our GPSTracker
        int geocoderMaxResults = 1;

        // The minimum distance to change updates in meters
        private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

        // The minimum time between updates in milliseconds
        private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

        // Declaring a Location Manager
        protected LocationManager locationManager;

        // Store LocationManager.GPS_PROVIDER or LocationManager.NETWORK_PROVIDER information
        private String provider_info;

        public GPSTracker(Activity context) {
            this.mContext = context;
            getLocation();
        }

        /**
         * Try to get my current location by GPS or Network Provider
         */
        public void getLocation() {

            try {
                locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);

                //getting GPS status
                isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                LocationServiceUtil.isGPSEnabled = isGPSEnabled;

                //getting network status
                isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                // Try to get location if you GPS Service is enabled
                if (isGPSEnabled) {
                    this.isGPSTrackingEnabled = true;

                /*
                 * This provider determines location using
                 * satellites. Depending on conditions, this provider may take a while to return
                 * a location fix.
                 */

                    provider_info = LocationManager.GPS_PROVIDER;

                } else if (isNetworkEnabled) { // Try to get location if you Network Service is enabled
                    this.isGPSTrackingEnabled = true;

                /*
                 * This provider determines location based on
                 * availability of cell tower and WiFi access points. Results are retrieved
                 * by means of a network lookup.
                 */
                    provider_info = LocationManager.NETWORK_PROVIDER;

                }

                // Application can use GPS or Network Provider
                if (!provider_info.isEmpty()) {
                    locationManager.requestLocationUpdates(
                            provider_info,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES,
                            this
                    );

                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(provider_info);
                        updateGPSCoordinates();
                    }
                }
            }
            catch (Exception e)
            {
                Log.e("LocationService", "Impossible to connect to LocationManager", e);
            }
        }

        /**
         * Update GPSTracker latitude and longitude
         */
        public void updateGPSCoordinates() {
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }
        }

        /**
         * GPSTracker latitude getter and setter
         * @return latitude
         */
        public double getLatitude() {
            if (location != null) {
                latitude = location.getLatitude();
            }

            return latitude;
        }

        /**
         * GPSTracker longitude getter and setter
         * @return
         */
        public double getLongitude() {
            if (location != null) {
                longitude = location.getLongitude();
            }

            return longitude;
        }

        /**
         * GPSTracker isGPSTrackingEnabled getter.
         * Check GPS/wifi is enabled
         */
        public boolean getIsGPSTrackingEnabled() {

            return this.isGPSTrackingEnabled;
        }

        /**
         * Stop using GPS listener
         * Calling this method will stop using GPS in your app
         */
        public void stopUsingGPS() {
            if (locationManager != null) {
                locationManager.removeUpdates(GPSTracker.this);
            }
        }

        /**
         * Function to show settings alert dialog
         */
        public void showSettingsAlert() {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

            //Setting Dialog Title
            alertDialog.setTitle(R.string.location_service_cancel_title);

            //Setting Dialog Message
            alertDialog.setMessage(R.string.location_service_cancel_message);

            //On Pressing Setting button
            alertDialog.setPositiveButton(R.string.action_settings, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which){
                    final int LOCATION_SETTINGS_TAG = 1;
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    mContext.startActivityForResult(intent, LOCATION_SETTINGS_TAG);
                }
            });

            //On pressing cancel button
            alertDialog.setNegativeButton(R.string.location_service_cancel_negative, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    dialog.cancel();
                    Toast.makeText(mContext,"Please turn on GPS setting to proceed ",Toast.LENGTH_LONG).show();
                }
            });

            alertDialog.show();
        }

        @Override
        public void onLocationChanged(Location location) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public IBinder onBind(Intent intent) {
            return null;
        }
    }
}

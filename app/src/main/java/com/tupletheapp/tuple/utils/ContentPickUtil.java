package com.tupletheapp.tuple.utils;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;

import com.tupletheapp.tuple.R;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ghigareda on 5/20/2015.
 */
public class ContentPickUtil {
    private static ContentPickUtil instance = null;

    protected ContentPickUtil(){

    }

    public static ContentPickUtil getInstance(){
        if(instance == null)
            instance = new ContentPickUtil();
        return instance;
    }

    public void startPhotoIntentActivity(Activity activity){
        activity.startActivityForResult(generatePhotoIntent(activity),
                activity.getResources().getInteger(R.integer.photo_request_code));
    }

    private Intent generatePhotoIntent(Activity activity){
        ArrayList<Intent> photoCaptureIntentList =
                generatePhotoCaptureIntents(activity.getPackageManager());

        Intent chooserIntent = Intent.createChooser(generatePhotoGalleryIntent(), "Select Source");

        chooserIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE,
                photoCaptureIntentList.toArray(new Parcelable[photoCaptureIntentList.size()]));

        return chooserIntent;
    }

    private ArrayList<Intent> generatePhotoCaptureIntents(PackageManager packageManager){
        ArrayList<Intent> cameraIntents = new ArrayList<>();
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);

        for (int i = 0; i < listCam.size(); i++){
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(listCam.get(i).activityInfo.packageName, listCam.get(i).activityInfo.name));
            intent.setPackage(listCam.get(i).activityInfo.packageName);
            cameraIntents.add(intent);
        }

        return cameraIntents;
    }

    private Intent generatePhotoGalleryIntent(){
        Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        return galleryIntent;
    }

    public void startVideoIntentFragment(Fragment fragment){
        Intent chooserIntent = startVideoIntent(fragment.getActivity());
        fragment.startActivityForResult(chooserIntent, fragment.getActivity().getResources().getInteger(R.integer.video_request_code));
    }

    public void startVideoIntentActivity(Activity activity){
        Intent chooserIntent = startVideoIntent(activity);
        activity.startActivityForResult(chooserIntent, activity.getResources().getInteger(R.integer.video_request_code));
    }

    private Intent startVideoIntent(Activity activity){
        String packageName;
        Intent intent;

        ArrayList<Intent> videoIntents = new ArrayList<>();
        Intent captureIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        PackageManager packageManager = activity.getPackageManager();
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (int i = 0; i < listCam.size(); i++){
            packageName = listCam.get(i).activityInfo.packageName;
            intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(listCam.get(i).activityInfo.packageName, listCam.get(i).activityInfo.name));
            intent.setPackage(packageName);
            videoIntents.add(intent);
        }

        Intent galleryIntent = new Intent();
        galleryIntent.setType("video/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, videoIntents.toArray(new Parcelable[videoIntents.size()]));

        return chooserIntent;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
}

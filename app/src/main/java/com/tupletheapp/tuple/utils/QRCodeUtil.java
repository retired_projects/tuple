package com.tupletheapp.tuple.utils;

import android.graphics.Bitmap;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

/**
 * Created by ghigareda on 2/2/2016.
 */
public class QRCodeUtil {
    public final static int WHITE = 0xFFFFFFFF;
    public final static int BLACK = 0xFF000000;
    private final int SIZE = 400;

    private static QRCodeUtil instance = null;

    protected QRCodeUtil(){

    }

    public static QRCodeUtil getInstance(){
        if(instance == null)
            instance = new QRCodeUtil();
        return instance;
    }

    public Bitmap generateQRCode(String objectId){
        BitMatrix result;

        try {
            result = new MultiFormatWriter().encode(objectId,
                    BarcodeFormat.QR_CODE, SIZE, SIZE, null);
        } catch (WriterException e) {
            return null;
        }

        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, w, 0, 0, w, h);
        return bitmap;
    }
}

package com.tupletheapp.tuple.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public class BitmapUtil {
    private static BitmapUtil instance = null;

    public BitmapUtil(){

    }

    public static BitmapUtil newInstance(){
        if (instance == null)
            instance = new BitmapUtil();
        return instance;
    }

    public Bitmap getRotatedBitmap(Bitmap originalBitmap, String imagePath){
        return  Bitmap.createBitmap(originalBitmap,
                0,
                0,
                originalBitmap.getWidth(),
                originalBitmap.getHeight(),
                getRotatedMatrix(imagePath),
                true
        );
    }

    public Bitmap getRotatedBitmap(String imagePath){
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, bounds);

        BitmapFactory.Options opts = new BitmapFactory.Options();
        Bitmap originalBitmap = BitmapFactory.decodeFile(imagePath, opts);

        return  Bitmap.createBitmap(originalBitmap,
                0,
                0,
                originalBitmap.getWidth(),
                originalBitmap.getHeight(),
                getRotatedMatrix(imagePath),
                true
        );
    }

    public Matrix getRotatedMatrix(String imagePath){
        ExifInterface exif;
        Matrix matrix = new Matrix();
        try {
            exif = new ExifInterface(imagePath);
            switch(exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1)){
                case ExifInterface.ORIENTATION_NORMAL:
                    matrix.postRotate(0);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.postRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.postRotate(270);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return matrix;
    }

    public byte[] getByteArray(Bitmap bitmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }
}
